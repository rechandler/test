/**
 * rpcConfig.js
 * A place where config options can be stored
 * created by: Ryann Chandler 7/25/17
 */

class AMQPConfig {


    
    // set up default values
    constructor() {
        
        this.DEPLOYED = false;
        this.LIFE_CYCLE = 'Sandbox';
        this.DOMAIN_NAME= 'localhost';
        this.APP_NAME = 'Workflow Service';
        
        this.AMQP_PROTOCOL = 'amqp';
        this.AMQP_HOST = 'amqp://localhost';
        this.AMQP_VIRTUALHOST = "%2F";
        this.AMQP_USERNAME = 'guest';
        this.AMQP_PASSWORD = 'guest';
        this.AMQP_SECURE = this.DEPLOYED;
        this.AMQP_TOPIC_EXCHANGE = 'amq.topic';
        this.AMQP_TOPIC_EXCHANGE_TYPE = 'topic';
        this.AMQP_TOPIC_EXCHANGE_DURABILITY = 'true';

        this.AMQP_QUEUE_NAME_ENGINE = 'Workflow.Engine.' + this.LIFE_CYCLE;
        this.AMQP_QUEUE_TIMEOUT_ENGINE = 'long';
        this.AMQP_QUEUE_DURABILITY_ENGINE = 'true';
        this.AMQP_QUEUE_ROUTE_KEY_ENGINE = "WorkflowEngine." + this.LIFE_CYCLE + ".#";
        this.AMQP_REQUEST_ROUTING_KEY_ENGINE = 'Request.' + this.LIFE_CYCLE + '.' + 'Dycom_Workflow_Engine';
        this.AMQP_RESPONSE_ROUTING_KEY_ENGINE = 'Response.' + this.LIFE_CYCLE + '.' + 'Dycom_Workflow_Engine';

        this.AMQP_QUEUE_NAME_WORKFLOWAPI = 'Workflow.API' + '.' + this.LIFE_CYCLE;
        this.AMQP_QUEUE_TIMEOUT_WORKFLOWAPI = 'long';
        this.AMQP_QUEUE_DURABILITY_WORKFLOWAPI = 'true';
        this.AMQP_QUEUE_ROUTE_KEY_WORKFLOWAPI = "WorkflowAPI." + this.LIFE_CYCLE + ".#";
        this.AMQP_REQUEST_ROUTING_KEY_WORKFLOWAPI = 'Request.' + this.LIFE_CYCLE + '.' + 'WorkFlow_API';
        this.AMQP_RESPONSE_ROUTING_KEY_WORKFLOWAPI = 'Response.' + this.LIFE_CYCLE + '.' + 'WorkFlow_API';

        this.AMQP_CONTENT_TYPE = 'application/json';

        this.AMQP_LOG_TOPIC = 'log';
        this.AMQP_LOG_TYPE = 'topic';
        this.AMQP_LOG_LOGGING = {
            info:'workflow.INFO', 
            warning: 'workflow.WARNING', 
            error: 'workflow.ERROR', 
            debug: 'workflow.DEBUG', 
            critical: 'workflow.CRITICAL'
        };
    }

    get amqp_settings() {
        return this.AMQP_SETTINGS;
    } 

    get domainName() {
        return this.DEPLOYED? 'layout-' + this.LIFE_CYCLE.toLowerCase() + '.services.certusview.com' : 'localhost';
    }

    get amqp_url() {
        return this.AMQP_PROTOCOL + '://' + this.DOMAIN_NAME + '?heartbeat=10';
    }
}

module.exports = AMQPConfig;