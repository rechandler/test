conn = new Mongo();
db = conn.getDB("dycom_workflow");

db.P6_activities_staging_approval.insert(
    {
        "_id" : ObjectId("59c13926c069ba311457c682"),
        "processName" : "P6_activities_staging_approval",
        "processId" : "219627",
        "parentToken" : null,
        "properties" : {
            "route" : "p6/activities",
            "params" : {
                "projectObjectId" : 9,
                "id" : "12345",
                "ObjectId" : 287492
            },
            "approver": "manager@sample.com",
            "submitter": "some_guy@sample.com"
        },
        "state" : {
            "tokens" : [ 
                {
                    "position" : "Decision",
                    "owningProcessId" : "219627"
                }
            ]
        },
        "history" : {
            "historyEntries" : [ 
                {
                    "name" : "Start",
                    "type" : "startEvent",
                    "begin" : 1505835300991.0,
                    "end" : 1505835300995.0
                }, 
                {
                    "name" : "StageData",
                    "type" : "task",
                    "begin" : 1505835300995.0,
                    "end" : 1505835302345.0
                }, 
                {
                    "name" : "Decision",
                    "type" : "intermediateCatchEvent",
                    "begin" : 1505835302345.0,
                    "end" : null
                }
            ],
            "createdAt" : 1505835300990.0,
            "finishedAt" : null
        },
        "pendingTimeouts" : {},
        "views" : {
            "startEvent" : {
                "name" : "Start",
                "type" : "startEvent",
                "begin" : 1505835300991.0,
                "end" : 1505835300995.0
            },
            "endEvent" : null,
            "duration" : null
        }
    }
);
