/**
 * AMQP MANAGER
 * This class is designed to be a single point for manageing an AMQP Server Connection
 * It's main responsibility is to manage the connection
 * It exposes a consumer module and a publisher module. 
 */

let amqp = require('amqplib');
let AMQPConsumer = require('./amqp_consumer/amqp-consumer');
let AMQPPublisher = require('./amqp_publisher/amqp-publisher');
let utils = require('./utils/AMQPUtils')
let _ = require('lodash');

class AMQPManager {
    /**
     * 
     * @param {Object} config Defines all properties for connecting to AMQP
     * @param {Function} consumerCallback Defines the function that is to be called when a message is recieved
     */
    constructor(config) {
        // set the config and consumeCallback
        this.config = config;

        // The queues successfully asserted via this instance of the manager and their options
        // { 'queueName': { queueOptions... }, ... }
        this.registeredQueueOptions = {};
        // The consumers successfully registered via this instance of the manager
        // { 'queueName': { 'consumerTag': consumerCbFunction }, ... }
        this.registeredConsumers = {};
    }

    // PUBLIC FUNCTIONS

    /**
     * Attempts to create a connection with the AMQP Server
     */
    connectToServer() {
        return this._setupConnection()
    }

    /**
     * Attempts to Create a channel.
     * Must be called after connectTOServer so a connection can be set
     */
    setupChannel() {
        if(!this.connection) {
            throw 'There is no Connection. Cannot Create Channel'
        }

        // Attempt to create a channel
        return this._createChannel(this.connection)
            .then((channel) => this._handleChannel(channel)); 
    }

    createExchange(topic, type) {
        return this.channel.assertExchange(topic, type);
    }

    addQueueBindings(queue, exchange, routingKey) {
        
        // queue and routingKey must be passed in
        if(!queue) {
            throw 'There is no queue. Cannot Create Binding';
        }
        if(!routingKey) {
            throw 'There is no routing key. Cannot Create Binding';
        }

        // if no exchange, use the default exchange
        if(!exchange) {
            exchange = this.config.AMQP_TOPIC_EXCHANGE;
        }


        return this.channel.bindQueue(queue, exchange, routingKey);
    }

    createQueue(queue) {
        return this.channel.assertQueue(queue);
    }

    /**
     * 
     * @param {String} queue - The name of the queue
     * @param {*} source - the exchange
     * @param {*} routingKey 
     */
    unbindQueue(queue, exchange, routingKey) {

         // if no exchange, use the default exchange
         if(!exchange) {
            exchange = this.config.AMQP_TOPIC_EXCHANGE;
        }

        return this.channel.unbindQueue(queue, exchange, routingKey);
    }

    unbindExchange(queue, exchange, routingkey) {

        if(!exchange) {
            exchange = this.config.AMQP_TOPIC_EXCHANGE;
        }

        return this.channel.unbindExchange(queue, exchange, routingKey)
    }

    /**
     * Deletes a queue
     * @param {String} queue - the name of the queue to delete
     */
    deleteQueue(queue) {
        return this.channel.deleteQueue(queue);
    }

    /**
     * Removes an exchange
     * @param {String} exchange - the exchange to delete
     */
    deleteExchange(exchange) {
        return this.channel.deleteExchange(exchange);
    }

    /**
     * Attempts to add a consumer using the previously set channel on the previously set connection
     */
    addConsumer(queue, queueOptions, consumerCallback) {

        if(!this.channel){
            throw 'There is no channel to consume';
        }

        if(!consumerCallback){
            throw 'There must be a consumer function to consume a queue';
        }

        // instantiate New Consumer Instance
        let consumer = new AMQPConsumer(this.channel, queue, consumerCallback, queueOptions);

        // Begin Consuming the queue
        return consumer.consumeQueue()
            .then((ok) => {
                // Set the registration properties for tracking
                this.registeredConsumers[queue] = consumer;
                this.registeredQueueOptions[queue] = queueOptions;
                
                return ok;
            });
    } 
    /**
     * Remove all registered instances of the given consumer from the given queue.
     * If the same consumer function has been registered on the queue multiple times, all instances will be removed.
     *
     * @param {string} queue - The name of the queue from which to remove the consumer
     * @param {Function} consumer - The consumer function that was added to the queue.
     *                            	This must reference the same function that was added, or the consumer will not be removed.
     * @returns {Promise.<boolean>} True if at least one consumer was removed. False if the given consumer function was not being used.
     */
    removeConsumer(queue) {
        
        if(!this.channel) {
            throw new Error('You are not currently connected to an AMQP server. Call AMQPManager#connectToServer first.');
        }
        if(!queue){
            throw new Error('A queue name must be given and cannot be an empty string.');
        }

        if(!this.registeredConsumers[queue]){
            return Promise.resolve(false);
        }

        let amqpConsumer = this.registeredConsumers[queue];

        // remove the consumer
        delete this.registeredConsumers[queue];
        
        if(!amqpConsumer) {
            throw 'Consumer Specified is not Found'
        };

        // Check The queue Exists before deleting
        return amqpConsumer.channel.checkQueue(amqpConsumer.queue)
            .then((ok) => {
                return amqpConsumer.channel.deleteQueue(amqpConsumer.queue);
            })
            .catch(err => logger.error(err));

    }

    /**
     * Expose a publishing Method
     */
    publish(topic, type, routingKey, msg) {
        let publisher = new AMQPPublisher(this.channel, {}, {});
        return publisher.publish(topic, type, routingKey, msg);
    }

    // PRIVATE FUNCTIONS

    /**
     * Wrapper for connecting and then handling the connection
     */
    _setupConnection(isReconnect) {

        let promise = this._connect()
            .then((connection) => this._handleConnection(connection));

        if(isReconnect){
            promise.then(() => {
                this._reCreateChannel();
            })
        }

        return promise
            .catch((err) => {
                console.error(err);
                setTimeout(() => {
                    this._setupConnection(isReconnect);
                }, 10000);
            });
    }
        

    /**
     * Creates the connection to the amqp Server
     * Uses a url found in the config set during instantiation.
     * 
     * calls amqp.connect to create the connection
     */
    _connect() {
        let url = this.config.amqp_url;

        if(!url) {
            throw 'Can Not connect to AMQP Server. No URL';
        }

        // create the connection. Returns a promise that resolves the connection object
        return amqp.connect(url);
    }

    /**
     * Once the connection to the amqp server has been successful, attempt to set up what happens on error.
     *   On Error, the we delete the connection and channel, and attempt to re-setup by starting at the beginning
     * @param {Object} connection The connection object that is returned from connecting to the amqp server
     */
    _handleConnection(connection) {
        // Set the connection
        this.connection = connection;

        // On error, decide what happens
        this.connection.on('error', (err) => {
            // Log out the error
            console.error('AMQPManager Connection Error:');
            console.error(err.stack || err);

            this.connection = null;
            this.channel = null;

            // re-run to restart the connection
            this._connect()
                .then((connection) => {
                    this.connection = connection;

                    // Recreate the channel
                    return this._reCreateChannel();
                })
                .catch((err) => {
                    setTimeout(() => {
                        this._setupConnection(true);
                    }, 10000);
                })
        });

        return Promise.resolve(connection);
    }

    // ### createChannel
    /**
     * Creates a new channel after connecting 
     * @param {Object} connection - the Connection returned from the connection 
     */
    _createChannel(connection) {

        // Save the connection and create the channel
        return connection.createChannel();
    }

    /**
     * Attempts to reacreate the channel and re establish the queues
     */
    _reCreateChannel() {

        // set up the channel
        return this.setupChannel()
            .then(() => {
                // Re-register all fo the consumers
                var promises = [];

                // Iterate through each queue with registered consumers and re-register them
                _.forOwn(this.registeredConsumers, (consumers, queue) => {
                    var queueConsumers = _.values(consumers);

                    for(let queue in this.registeredConsumers) {
                        let amqpConsumer = this.registeredConsumers[queue];
                        
                        promises.push(this.addConsumer(amqpConsumer.queue, amqpConsumer.queueOptions, amqpConsumer.consumer));
                     }
    
                });

                // return all the promises once they resolve
                return Promise.all(promises)
            })
            .then(() => {
                // TODO: Loggin?
                console.log('All Queues Re Created');
            });
    }

    /**
     * Handles the errors for a channel for an AMQP Channel
     * @param {Objet} channel - The channel that was created by calling createChannel
     */
    _handleChannel(channel) {

        this.channel = channel;

        // Set the prefetch count based on the "messagesPerConsumer" option
        this.channel.prefetch(this.config.messagesPerConsumer);

        // On error, decide what happens
        this.channel.on('error', (err) => {
            console.error('AMQPManager Channel Error:');
            console.error(err.stack || err);

            // nullify the channel
            this.channel = null;

            // Recreate the channel
            return this._reCreateChannel();
        });

        return Promise.resolve(this.channel);
    }

    // GETTERS AND SETTERS

    get config() {
        return this._config;
    }
    set config(config) {
        this._config = config;
    }

    get channel() {
        return this._channel;
    }
    set channel(channel) {
        this._channel = channel;
    }

    get connection() {
        return this._connection;
    }
    set connection(connection) {
        this._connection = connection;
    }

}

module.exports = AMQPManager;