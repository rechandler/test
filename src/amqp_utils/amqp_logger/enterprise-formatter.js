const AMQPConfig = require('../../amqp_config/amqpConfig');
const dns = require('dns');
const os = require('os');

let config = new AMQPConfig();

exports.formatEnterpriseLog = (message, level, module, details) => {
        return getIpAddress()
            .then((ip) => {
                return {
                    "application": config.APP_NAME,
                    "application_version": "1.0.0",
                    "attachments": "No Attachments",
                    "application_module": module,
                    "log_type": "logs",
                    "log_index": "workflow-" + config.LIFE_CYCLE,
                    "level": level,
                    "message": message,        
                    "message_details": details,
                    "server": config.DOMAIN_NAME,
                    "client_ip": ip,
                    "timestamp": new Date()
                };
            })
}

/**
 * Gets the curren IP of the host server
 */
getIpAddress = () => {
    return new Promise((resolve, reject) => {
        dns.lookup(os.hostname(),  (err, address) => {
            // throw an errer
            if(err) {
                reject(err);
            }
    
            resolve( address );
        });
    });
}