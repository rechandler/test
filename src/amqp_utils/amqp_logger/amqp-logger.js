const AMQPConfig = require('../../amqp_config/amqpConfig');
const AMQPManager= require('../amqp-manager');
const formatter = require('./enterprise-formatter');


class AMQPLogger {

    constructor() {

        // build a new config
        let amqpConfig = new AMQPConfig();

        // set the logging information
        this.topic = amqpConfig.AMQP_LOG_TOPIC;
        this.type = amqpConfig.AMQP_LOG_TYPE;
        this.logInfo = amqpConfig.AMQP_LOG_LOGGING;

        this.amqp_manager = new AMQPManager(amqpConfig);
    
        
        this.amqp_manager.connectToServer()
            .then(() => this.amqp_manager.setupChannel())
            .then(() => this.amqp_manager.createExchange(this.topic, this.type))
            .then(() => this.setupLoggingQueues());
    }

    /**
     * Sets up all of our logging queues and binds to the appropriate exchange.
     * In this case, for binding, the routing key and the queue are the same
     */
    setupLoggingQueues() {
        var promises = [];
        // Create the queue's and routing keys and bind them together
        for(let key in this.logInfo){

            // grab the queue name
            let queue = this.logInfo[key];

            let setupPromise = this.amqp_manager.createQueue(queue)
                .then(() => this.amqp_manager.addQueueBindings(queue, this.topic, queue))    
                .then(() => console.log('queue created', queue));

            promises.push(setupPromise);
        }

        return Promise.all(promises);
    }

    /**
     * Publishes a message to our logging queues
     * @param {Object|String} message - a message to publish
     * @param {String} routingKey - routing Key
     */
    log(message, routingKey, level, module, stack){

        return formatter.formatEnterpriseLog(message, level, module, stack)
            .then((formattedMessage) => {
                return this.amqp_manager.publish(this.topic, 
                    this.type, 
                    routingKey, 
                    formattedMessage);
            });
    }

    /**
     * Logs a message as informational
     * @param {Object} message - the message to log
     */
    info(message, module) {
        console.info(message);
        return this.log(message, this.logInfo.info, 'info', module, null);
    }

   /**
     * Logs a message as a warning
     * @param {Object} message - the message to log
     */
    warn(message, module) {
        return this.log(message, this.logInfo.warn, 'warn', module, null);        
    }

    /**
     * Logs a message as an error
     * @param {Object} message - the message to log
     */
    error(message, module) {
        console.error(message);
        return this.log(message, this.logInfo.error, 'error', module, message.stack);        
    }

    /**
     * Logs a message as debugging
     * @param {Object} message - the message to log
     */
    debug(message, module) {
        return this.log(message, this.logInfo.debug, 'debug', module, null);
    }

    /**
     * Logs a message as critical
     * @param {Object} message - the message to log
     */
    critical(message, module) {
        return this.log(message, this.logInfo.critical, 'critical', module, null);        
    }

    /**
     * prints out a stack trace
     * @param {Objcet} message - message to trace
     */
    trace(message) {
        console.trace('DEBUG: ', message);         
    }


    // GETTERS AND SETTERS //
    get amqp_manager() {
        return this._amqp_manager;
    }

    set amqp_manager(amqp_manager) {
        this._amqp_manager = amqp_manager;
    }

    get topic() {
        return this._topic;
    }
    set topic(topic) {
        this._topic = topic;
    }

    get type() {
        return this._type;
    }
    set type(type) {
        this._type = type;
    }
}

module.exports = AMQPLogger;