let _ = require('lodash');
let	amqpConnectionLib = require('amqplib/lib/connection');

/**
 * Handle an enexpected error thrown during AMQP operations
 * @param {Error} err - The error thrown by AMQP
 * @param {string} action - A description of the action being performed when the error occurred
 * @throws {Error} A wrapped version of the error object
 */
exports.handleError = (err, action, connection, channel) => {
    // Add additional information to the error
    let extendedErr = _.extend(err, {
        connection: connection,
        channel: channel,
        fatal: amqpConnectionLib.isFatalError(err)
    });

    // Log the info to the console
    console.error('Unexpected AMQP Error while ' + action + ': ', extendedErr, extendedErr.stack);

    // Throw the error to make sure any affected promise chains are rejected
    throw extendedErr;
}

exports.uuid = () => {
    let s4 = () => {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
    }

    return this.s4() + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + '-' + this.s4() + this.s4() + this.s4();
}