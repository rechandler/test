'use strict';

const chai = require('chai');
const expect = chai.expect;
const should = chai.should;
const assert = chai.assert;
const sinon = require('sinon');
const amqp = require('./amqp-consumer');


describe('AMQP Consumer Module', function(){

    let amqpTester;
    beforeEach(function(){
        amqpTester = new amqp(utils.channel, 'test', () => true, {'rpc_settings': {'id': '1', 'AMQP_QUEUE_NAME': '2'}});
    });

    describe('Instantiation', function(){
        it('Should be instantiated', () => {
            expect(amqpTester).to.be.a('Object');
            expect(amqpTester.channel).to.be.a('Object');
            expect(amqpTester.queue).to.be.a('String');
            
        });
    });

    describe('Consuming a queue', function(){
        it('should start a consumer on a queue', function(done){
            let dataCallback = sinon.stub(amqpTester, '_getDataFromMessage').returns(true);

            amqpTester.consumeQueue()
                .then((ok) => {
                    expect(ok).to.deep.equal({consumerTag: '123'});
                    done();
                });
        });

        it('should get data from a message', function() {
            let val = new Buffer(JSON.stringify({id: 'test_7', isApproved: true, message: 'testing'}));
            let message = {content: val};

            let result = amqpTester._getDataFromMessage(message);
            expect(result).to.deep.equal({id: 'test_7', isApproved: true, message: 'testing'});
        });
    });

    describe('Acknowledgers', function() {
        it('should provide ack function', function() {
            expect(amqpTester.consumerAck()).to.be.a('Function');
        });

        it('should provide nack function', function() {
            expect(amqpTester.consumerNack()).to.be.a('Function');
        });
    });
});


let utils = {
    channel: {
        assertQueue: () => Promise.resolve(),
        consume: () => Promise.resolve({consumerTag: '123'})
    },
    connection: {
        on: () => Promise.resolve(),
        createChannel: () => true
    },
    registeredQueueOptions: { 'queueName': { queueOptions: 'none' } },
    registeredConsumers: { 'queueName': { 'consumerTag': () => true } }
}