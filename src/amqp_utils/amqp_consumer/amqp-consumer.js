/**
 * AMQP CONSUMER
 * This module manages the consmer module for consuming AMQP queues.
 */

// requires
let _ = require('lodash');
let amqp = require('amqplib');

/**
 * AMQPConsumer class
 */
class AMQPConsumer {

    /**
     * Instantiate an instance of AMQPConsumer
     * @param {Object} channel - the channel object created by the manager
     * @param {String} queue - the name of the queue to consume
     * @param {Function} consumer - the consuming function to be run when a message is recieved
     * @param {Object} queueOptions - any extra options to use when connecting to a queue
     */
    constructor(channel, queue, consumer, queueOptions) {
        this.channel = channel;
        this.queue = queue;
        this.consumer = consumer;
        this.queueOptions = queueOptions || {};
    }

    // PUBLIC FUNCTIONS

    /**
     * Instantiates a consumer on a queue
     * @returns {Promise} ok - Once the queue is consumed we return the object
     */
    consumeQueue() {

        // Make sure all necessary items are available
        if (!this.channel) {
            throw new Error('You are not currently connected to an AMQP server. Call AMQPManager#connectToServer first.');
        }
        if (!this.queue) {
            throw new Error('A queue name must be given and cannot be an empty string.');
        }
        if (!this.consumer || !_.isFunction(this.consumer)) {
            throw new Error('A consumer function must be given when adding a consumer to queue.');
        }

        // set any extra queue options
        this.queueOptions = _.extend({
            durable: true,
            exclusive: false
        }, this.queueOptions);

        // Assert the queue
        return this.channel.assertQueue(this.queue, this.queueOptions)
            .then(() => {

                // Then set the channel to consume it
                return this.channel.consume(this.queue, (message) => {

                    // make sure there is a message
                    if(!message) return;

                    // get the data from the message
                    let data = this._getDataFromMessage(message);
 
                    // Use the consumer function (passed in from instantiating the queue manager)
                    //  to take further action on the message
                    this.consumer(
                        data, 
                        this.consumerAck(message), 
                        this.consumerNack(message));
                });
            })
            .then((ok) => {
                return Promise.resolve(ok);
            });
    }

    /**
     * Acknowledge the message
     * @param {Object} message - the message recieved via amqp 
     */
    consumerAck(message) {
        return () => {
            return this.channel.ack(message);
        }
        
    }

    /**
     * Not Acknowldge the message
     * @param {Object} message - the message recieved via amqp
     */
        
    consumerNack(message) {
        return () => {
            return this.channel.nack(message);
        }
    }

    // PRIVATE FUNCTIONS

    // ### getDataFromMessage
    /**
     * returns a JSON object from the content of an amqp message
     * @param {*} message  - message recieved from amqp 
     */
    _getDataFromMessage(message) {
        let content = message.content;
        let content_string = content.toString();
        return JSON.parse(content_string);
    }

    // GETTERS AND SETTERS

    get channel() {
        return this._channel;
    }
    set channel(channel) {
        this._channel = channel;
    }

    get queue() {
        return this._queue;
    }
    set queue(queue) {
        this._queue = queue;
    }

    get queueOptions() {
        return this._queueOptions;
    }
    set queueOptions(queueOptions) {
        this._queueOptions = queueOptions;
    }
}

module.exports = AMQPConsumer