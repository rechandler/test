'use strict';

const chai = require('chai');
const expect = chai.expect;
const should = chai.should;
const assert = chai.assert;
const sinon = require('sinon');
const AMQPManager = require('./amqp-manager');

describe('AMQPManager Tests', function() {
    
    let manager;
    beforeEach(function() {
        manager = new AMQPManager({id: '1234', AMQP_QUEUE_NAME:'queue'}, function(){return true;});
    });

    describe('Module Instantiation', function() {
        it('should be initialized', function() {
            expect(manager).to.be.a('Object');
        });
    });

    describe('Connecting To Server', function() {
        it('should call connect and handler functions', function() {
            let connectorCallback = sinon.stub(manager, '_connect').resolves(true);
            let handlercallback = sinon.stub(manager, '_handleConnection').resolves(true);

            manager.connectToServer();

            expect(connectorCallback.called);
            expect(handlercallback.called);
        });
    });

    describe('Setting up the Channel', function() {
        it('Should call the create and handle functions', function() {
            let connectorCallback = sinon.stub(manager, '_createChannel').resolves(true);
            let handlercallback = sinon.stub(manager, '_handleChannel').resolves(true);
        
            manager.connection = 'connected';
            manager.setupChannel();

            expect(connectorCallback.called);
            expect(handlercallback.called);
        });

        it('Should handle the channel', function(done){
            let val = manager._handleConnection(utils.connection);

            val.then((ok) => {
                expect(ok).to.deep.equal(utils.connection);
                done();
            })
        });

        it('Should call createChannel', function(){
            let val = manager._createChannel(utils.connection);

            expect(val).to.equal(true);
        });
    });

    describe('Adding a consumer', function() {
        it('Should add a new consumer', function(done) {

            manager.channel = utils.channel;
            manager.consumerCallback = () => {return true;};

            manager.addConsumer('testQueue', {}, () => true)
                .then((ok) => {
                    expect(ok).to.deep.equal({consumerTag: '123'});
                    done();
                });
        })
    });

    describe('Remove a consumer', function(){
        it('Should remove a consumer', function(done){
            manager.channel = utils.channel;
            manager.registeredConsumers = Object.assign({}, utils.registeredConsumers);
            
            manager.removeConsumer('testQueue')
                .then(() => {

                    
                    expect(manager.registeredConsumers).to.deep.equal({otherQueue: {}});
                    done();
                });
        });
    });

    describe('Recreating the Channel', function() {
        it('should attempt to recreate the channel', function(done) {
            let spy = sinon.stub(manager, 'setupChannel').resolves(true);

            manager.registeredConsumers = {testQueue: Object.assign({}, utils.registeredConsumers.testQueue)};
            manager.registeredConsumers = utils.registeredConsumers;
            manager.connection = utils.connection;
            manager.channel = utils.channel;

            manager._reCreateChannel()
                .then((ok) => {
                    expect(spy.called);
                    done();     
                });

            expect(spy.called);
        });
    });
});

let utils = {
    channel: {
        assertQueue: () => Promise.resolve(),
        consume: () => Promise.resolve({consumerTag: '123'}),
        cancel: () => Promise.resolve(true)
    },
    connection: {
        on: () => Promise.resolve(),
        createChannel: () => true
    },
    registeredQueueOptions: { 'queueName': { queueOptions: 'none' } },
    registeredConsumers: { 
        testQueue: {
            channel: {
                checkQueue: (queue) => Promise.resolve(),
                deleteQueue: (queue) => Promise.resolve()
            },
            consumer: () => true,
            queue: 'testQueue',
            queueOptions: {}
        },
        otherQueue: {

        }
     }
}