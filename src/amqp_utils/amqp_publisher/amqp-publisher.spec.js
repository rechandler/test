'use strict';

const chai = require('chai');
const expect = chai.expect;
const should = chai.should;
const assert = chai.assert;
const sinon = require('sinon');


const amqpPublisher = require('./amqp-publisher');

let amqpTester;
beforeEach(function(){
    amqpTester = new amqpPublisher(utils.channel, utils.registeredQueueOptions, {'rpc_settings': {'id': '1'}});
});  

describe('AMQP Publisher Module', function(){
    describe('Instantiation', function() {
        it('should be instantiated', function(){
            expect(amqpTester).to.be.a('Object');
            expect(amqpTester.publish).to.be.a('Function');
            expect(amqpTester.options).to.be.a('Object');
        });
    });

    describe('Publishing', function() {
        it('should publish a message', function(done){

            amqpTester.publish('testTopic', null, 'testRoutingKey', {id: 'test_7', isApproved: true, message: 'testing'})
                .then((ok) => {
                    expect(ok).to.equal(true);
                    done();
                });
        });
    });
});

let utils = {
    channel: {
        assertExchange: () => Promise.resolve(),
        publish: () => true
    },
    registeredQueueOptions: { 'queueName': { queueOptions: 'none' } },
}