/**
 * AMQP PUBLISHER
 * This is a publisher module for AMQP.
 */

let _ = require('lodash');
let utils = require('../utils/AMQPUtils');

class AMQPPublisher {
/**
     * 
     * @param {*} config - the RPC config 
     */
    constructor(channel, registeredQueueOptions, options){

        // set the channel
        this.channel = channel;
        this.registeredQueueOptions = registeredQueueOptions;
        this.options = options;
    }

    // PUBLIC FUNCTIONS

    // ### publish
    /**
     * Publishes to a channel and queue
     * @param {*} queue - the name of the queue to publish to 
     */
    publish(topic, type, routingKey, message, queueOptions, messageOptions) {

        if(!this.channel) {
            throw new Error('You are not currently connected to an AMQP server. Call AMQPManager#connectToServer first.');
        }

        if(typeof message === 'undefined') {
            throw new Error('A message must be given and cannot be undefined.');
        }

        if(!topic) {
            throw new Error('No Topic as been definded');
        }
    
        if(!type){
            type = 'topic';
        }

        // Set the default values for the message options
        messageOptions = _.extend({
            persistent: true,
            content_type: 'application/json'
        }, messageOptions);

        // set exchange options
        let topicOptions = {
            durable: true
        }

        // assert the exchange
        return this.channel.assertExchange(topic, type, topicOptions)
            .then(() => {

                // Register this queue with the options used to declare it
                //this.registeredQueueOptions[queue] = queueOptions;

                let messageSent = false;
                let retrys = 0;
                let maxRetrys = this.options.retrys || 3;

                // Attempt to send the message until either it sends successfully or we max out our set retry attempts
                while (!messageSent && retrys <= maxRetrys) {
                    console.log('\n[*] Publishing to', routingKey);
                    if (topic === 'topic') {
                        topic = 'amq.topic';
                    }
                        messageSent = this.channel.publish(topic, routingKey, new Buffer(JSON.stringify(message)), {headers: message.headers || {}});
                    ++retrys;
                }

                // Throw an error if the message was not able to be published successfully
                if (!messageSent) {
                    throw new Error('Unable to publish message to the queue '+ topic + '. The write buffer may be full.');
                }

                return true;
            })
            .catch((err) => utils.handleError(err,'publishing a message to queue ' + topic.toString()));          
    }
    
    // GETTERS AND SETTERS

    get channel() {
        return this._channel;
    }

    set channel(channel) {
        this._channel = channel;
    }

    get registeredQueueOptions() {
        return this._registeredQueueOptions;
    }

    set registeredQueueOptions(options) {
        this._registeredQueueOptions = options;
    }

    get options() {
        return this._options;
    }

    set options(options) {
        this._options = options;
    }
}

module.exports = AMQPPublisher; 