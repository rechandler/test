/**
 * index.js
 * Entry point for AMQP
 * Initializes a consumer.
 * created by Ryann Chandler 7/25/17
 */

 //TODO: Enterprise Logging

 // Requires

let amqpConfig = require('../amqp_config/amqpConfig');
let AMQPManager = require('../amqp_utils/amqp-manager');
let bpmn = require('../workflow-modules/bpmn/runner/bpmn-runner');

// instantiate new config and consumer modules
let config = new amqpConfig();

/**
 * We pass this function into the AMQP manager. This allows for a more
 * flexible amqp manager. This function is run when a message is picked up
 * off of a queue. We control the logic for the workflow Service here.
 * @param {*} message 
 * @param {*} ack 
 * @param {*} noAck 
 */
let consumerCallback = (message, ack, noAck) => {

    // instantiate BPMN
    let bpmnRunner = new bpmn();

    // acknowledge the message
    ack(message);
    bpmnRunner.run(message)
        .then(() => {
            console.log('Finished Running Workflow');

            delete bpmnRunner;
        })
        .catch(err => logger.error(err));
}

let amqp_manager = new AMQPManager(config);

amqp_manager.connectToServer()
    .then(() => amqp_manager.setupChannel())
    .then(() => amqp_manager.createExchange(config.AMQP_TOPIC_EXCHANGE, config.AMQP_TOPIC_EXCHANGE_TYPE))
    .then(() => amqp_manager.createQueue(config.AMQP_QUEUE_NAME_ENGINE))
    .then(() => amqp_manager.addQueueBindings(config.AMQP_QUEUE_NAME_ENGINE, config.AMQP_TOPIC_EXCHANGE, config.AMQP_QUEUE_ROUTE_KEY_ENGINE))
    .then(() => amqp_manager.addConsumer(config.AMQP_QUEUE_NAME_ENGINE, {}, consumerCallback))
    .catch((err) => logger.error(err));
