/**
 * amqp-api.js
 * Entry point for AMQP API
 * Initializes a consumer.
 * created by Ryann Chandler 7/25/17
 */

 //TODO: Enterprise Logging

 // Requires

let amqpConfig = require('../amqp_config/amqpConfig');
let AMQPManager = require('../amqp_utils/amqp-manager');
let Factory = require('../workflow-modules/api-factory/factory');
let _ = require('lodash');

// instantiate new config and consumer modules
let config = new amqpConfig();

// instantiate utils
let utils = require('../workflow-modules/utils/utils');

/**
 * We pass this function into the AMQP manager. This allows for a more
 * flexible amqp manager. This function is run when a message is picked up
 * off of a queue. We control the logic for the workflow Service here.
 * @param {*} message 
 * @param {*} ack 
 * @param {*} noAck 
 */
let consumerCallback = (message, ack, noAck) => {

    // acknowledge the message
    ack(message);

    // Get the Method and Type
    // Type is a specifier that will help determine what action to take in the factory
    let method = _.get(message, 'method');
    let resource = _.get(message, 'params.resource');

    // A method is needed to proceed execution
    if(!method) {
        let err = 'Error, No Method for accessing workflow information'
        logger.error(err);
        throw err;
    }

    if(!resource) {
        let err = 'Error, No Typefor accessing workflow information';
        logger.error(err);
        throw err;
    }

    // result promise will always need to be a promise
    let resultPromise;
    switch(method) {
        case 'GET':
            resultPromise = new Factory(amqp_manager).get(resource, message);
            break;
        case 'POST':
            resultPromise = new Factory(amqp_manager).post(resource, message);
            break;
        case 'PUT':
            resultPromise = new Factory(amqp_manager).put(resource, message);
            break;
        case 'DELETE':
            resultPromise = new Factory(amqp_manager).delete(resource, message);
            break;
        default:
            throw 'No Method to perform';
            break;
    }

    resultPromise
        .then((result) => {

            // set the Response Topic
            let responseInfo = message.replyTo;

            if(responseInfo) {
                let routing = utils.getRouting(responseInfo);
                result.headers = _.merge(result.headers, responseInfo.headers);
                amqp_manager.publish(routing.topic, null, routing.routingKey, result || {});
            }
        })
        .catch(err => logger.error(err));
}

let amqp_manager = new AMQPManager(config);

amqp_manager.connectToServer()
    .then(() => amqp_manager.setupChannel())
    .then(() => amqp_manager.createExchange(config.AMQP_TOPIC_EXCHANGE, config.AMQP_TOPIC_EXCHANGE_TYPE))
    .then(() => amqp_manager.createQueue(config.AMQP_QUEUE_NAME_WORKFLOWAPI))
    .then(() => amqp_manager.addQueueBindings(config.AMQP_QUEUE_NAME_WORKFLOWAPI, config.AMQP_TOPIC_EXCHANGE, config.AMQP_QUEUE_ROUTE_KEY_WORKFLOWAPI))
    .then(() => amqp_manager.addConsumer(config.AMQP_QUEUE_NAME_WORKFLOWAPI, {}, consumerCallback))
    .catch((err) => logger.error(err));
