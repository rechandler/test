/*global module exports console */
let requestHandler = require('../workflow-modules/workflow-request/workflow-request');
let amqpConfig = require('../amqp_config/amqpConfig');
let _ = require('lodash');
let OVSTemplate = require('../workflow-modules/workflow-request/templates/OVS.template');

exports.startEvent = function( data , done ) {
    // called after the start event arrived at MyStart
    console.log("\nStart: ", data, "\n");

	done(data);
};

exports.Stage_Data = function( data, done) {
    console.log('Stage Data: A form has been submitted.');
    console.log('\tThis can be a task for staging the data');

    // create the request creator with the necessary amqp details
    let request_handler = new requestHandler('Stage_Data', OVSTemplate);

    // Merge all of the request data
    let requestData = _.merge({route: 'testRoute'}, {method: 'POST'}, {data: data});

    // Form the amqp request
    let request = request_handler.createWorkflowRequest(requestData);

    // could possibly check for required fields--

    // publish the request
    request_handler.publish(request)
        .then(() => {
            console.log('Published from task');
            this.taskDone('Stage Data', data);
        });
}

exports.Stage_DataDone = function( data, done) {
    console.log('\tDone Submitting Form.\n');

    done(data);   
}

exports.Approval = function( data , done ) {
    // called at the beginning of MyTask
    console.log("Approval Data: ", data);
 
    if(data.isApproved){
        console.log('\tLooks Good, The approval is done\n');
        done(data);
    } else {
        console.log('\tNot Approved');
    }
};  

exports.Release_Data = function( data, done ) {
    console.log('Release Data: The data has been approved.'); 
    console.log('\tHere we can release the data and end the workflow');
    this.taskDone('Release Data', data);
}

exports.Release_DataDone = function( data , done ) { 
    // Called after the process has been notified that the task has been finished
    // by invoking myProcess.taskDone("MyTask").
    // Note: <task name> + "Done" handler are only called for
    // user tasks, manual task, and unspecified tasks
    console.log("\tData has been Released: ", "\n");  
    done();
};

exports.End = function(data, done ) {
    console.log('End');
    done(data); 
}



/**
 * @param {String} eventType Possible types are: "activityFinishedEvent", "callHandler"
 * @param {String?} currentFlowObjectName The current activity or event
 * @param {String} handlerName
 * @param {String} reason Possible reasons:
 *                          - no handler given
 *                          - process is not in a state to handle the incoming event
 *                          - the event is not defined in the process
 *                          - the current state cannot be left because there are no outgoing flows
 */
exports.defaultEventHandler = function(eventType, currentFlowObjectName, handlerName, reason, done) {
    // Called, if no handler could be invoked.
    //console.log('default');
    done(reason);
};

exports.defaultErrorHandler = function(error, done) {
    // Called if errors are thrown in the event handlers
    console.log('default error');
    console.log(error);
    done();
};

exports.onBeginHandler = function(currentFlowObjectName, data, done) {
    // do something
    //console.log('begin handler: ', data);
    done(data);
};

exports.onEndHandler = function(currentFlowObjectName, data, done) {
    // do something
    //console.log('end handler');
    done(data);
}; 