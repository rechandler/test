/*global module exports console */
let AMQPManager = require('../amqp_utils/amqp-manager');
let _ = require('lodash');
let rpcUtils = require('../workflow-modules/utils/rpc-utils');
let p6Utils = require('../workflow-modules/utils/p6-activity-utils');
let utils = require('../workflow-modules/utils/utils.js');

// import requestCreator
let requestHandler = require('../workflow-modules/workflow-request/workflow-request');
let OVSTemplate = require('../workflow-modules/workflow-templates/OVS.StagedData');

exports.Start = function( data , done ) {
    try {
        logger.info("\nStart: " + JSON.stringify(data) + "\n", "Workflow.Engine");

        let workflowParams = data.params;
        let replyTo = data.replyTo;
        let submitter = replyTo.headers? replyTo.headers.user: null;

        // Set properties
        // Create unique uuid
        this.setProperty('route', data.route);
        this.setProperty('params', workflowParams);
        done(data); 
    } catch (err) {
        p6Utils.handleGeneralException(data, this, err, "Error During Start: ");
        done(data); 
    }
}

exports.ErrorResubmission = function( data, done) {
    try {
        logger.info('Error Resubmission', "Workflow.Engine");
        let err = this.getProperty('error');

        // If there is an error, remove it to try and restage data
        if(err){
            this.setProperty('error', null);
            done(data);
        } else {
            logger.info('error', "Workflow.Engine")
        }
    } catch (err) {
        p6Utils.handleGeneralException(data, this, err, "Error During Error Resubmission: ");
        done(data); 
    }
}

exports.StageData = function( data, done ) {
    
    let method = 'POST';
    let QUEUE_NAME = 'StagingData';
    let errorMessage = "Error Submitting Data for Staging: ";
    try {
        logger.info('StageData: Submiting call to OVS to Stage data.', "Workflow.Engine");
        logger.info('\tPublishing Stage data message', "Workflow.Engine");  

        let replyTo = data.replyTo;
        let submitter = replyTo && replyTo.headers? replyTo.headers.user: null;   
        let headers = data.headers;
        let approver = headers.approver;        

        let rpcConnectionInfo = rpcUtils.rpcConnectionInfo(QUEUE_NAME);
        
        // create the template
        // request template is imported, use its GET template
        let template = rpcUtils.createTemplate( OVSTemplate[method], 
                                                rpcConnectionInfo.queue, 
                                                rpcConnectionInfo.routingKey, 
                                                rpcConnectionInfo.exchange,
                                                data);

        // Create request handler first
        let request_handler = new requestHandler(template);

        // Consuming function of the rpc call
        let consumer = (message, ack, noAck) => {
            ack(message);

            let stagingId = message.id;

            // set the staging id in the workflow for joining with staged data later
            this.setProperty('id', stagingId);

            // fill in values
            request_handler.rpcCleanup(rpcConnectionInfo.queue, 
                                    rpcConnectionInfo.exchange.topic, 
                                    rpcConnectionInfo.routingKey, 
                                    consumer)
                .then(() => {
                    let responseMessage = p6Utils.formatResponseMessage(message, this, errorMessage);

                    // If we have replyTo information, reply
                    if(replyTo){
                        request_handler.reply(replyTo, responseMessage);
                    }

                    // save approver and submitter after staging data.
                    this.setProperty('submitter', submitter);   
                    this.setProperty('approver', approver);
                    
                    let id = this.getProperty('id');
                    if(!id) {
                        this.setProperty('error', "Error getting id property: not found");
                    }

                    // The task is complete
                    this.taskDone('StageData', message);
                });
        };

        // The message needs to be formatted a certain way for it to be staged.
        // This formats it.
        let dataToStage = p6Utils.formatDataForStaging(data, 'Submitted');

        // Format the data to populate the request template
        let requestData = rpcUtils.formatDataForStageTemplating(dataToStage, method, rpcConnectionInfo);

        // begin the rpc call
        request_handler.rpcCall(consumer, requestData);
    } catch (err) {
        p6Utils.handleGeneralException(data, this, err, errorMessage);
        this.taskDone('StageData', data);
    }
};

exports.StageDataDone = function( data , done ) { 
    // Called after the process has been notified that the task has been finished
    // by invoking myProcess.taskDone("MyTask").
    // Note: <task name> + "Done" handler are only called for
    // user tasks, manual task, and unspecified tasks
    logger.info("\tStage Data task has been completed" + ( this.getProperty('error') ? ' with errors' : '' ) + ": \n", "Workflow.Engine");  
    done(data);
};

exports.StagingError = function( data, done) {
    done(data);
}

exports.StagingError$Error = function( data, done) {
    let error = this.getProperty('error');

    if(error) {
        logger.info('error Found', "Workflow.Engine");
        return true;
    } else {
        return false;
    }
}

exports.StagingError$false = function( data, done) {
    let error = this.getProperty('error');

    if(error) {
        return false;
    } else {
        logger.info('no error', "Workflow.Engine")
        return true;
    }
}

exports.Decision = function( data , done ) {
    // called at the beginning of MyTask
    logger.info("Decision event: Make a decision on what to do next in workflow", "Workflow.Engine")
    logger.info("\tDecision Data: ", "Workflow.Engine")
    logger.info("\t" + JSON.stringify(data), "Workflow.Engine");

    try {
        
        var replyTo = data.replyTo;
        
        // the data will be a rabbit message so rename for clarity;
        let message = data;
    
        let staging_status = message.data? message.data.staging_status: null;

        // clear out any previous errors
        let error = this.getProperty('error');
        if(error) {
            logger.info('Removing Previous Errors', "Workflow.Engine");
            this.setProperty('error', null);
            done(data);
            return;
        }

        if(!staging_status){
            if(replyTo){
                let response = {
                    status_code: 409,
                    message: 'This Activity Data is already Pending Approval. Can\'t resubmit until data is approved or rejected.'
                }
                
                // Create request handler first
                // No need for params. If no template is provided, it will use the default templates
                let request_handler = new requestHandler();

                request_handler.reply(replyTo, response);
            }
        } else if(staging_status === 'Rejected' || staging_status === 'Approved') {
            logger.info('\tLooks Good, The Decision is done\n', "Workflow.Engine");
            done(data);
        } else {
            logger.info('\tNot Decided, Nothing to do here', "Workflow.Engine");

            if(replyTo){

                // Create request handler first
                // No need for params. If no template is provided, it will use the default templates
                let request_handler = new requestHandler();

                request_handler.reply(replyTo, message);
            }
        }
    } catch (err) {
        p6Utils.handleGeneralException(data, this, err, "Error During Decision Gateway: ");
        done(data); 
    }
}; 

exports.Gateway = function( data , done ) {
    done(data);
}; 

exports.Gateway$Approved = function( data ) {
    try {
        logger.info("Gateway$Approved: " + JSON.stringify(data));
        let message = data;

        if(message.data.staging_status === 'Approved'){
            logger.info('\tLooks Good, The staged data is Approved\n', "Workflow.Engine");
            return true;
        } else {
            logger.info('\tNo Good, do nothing', "Workflow.Engine");
            return false;
        }
    } catch (err) {
        p6Utils.handleGeneralException(data, this, err, "Error During Gateway$Approved: ");
        done(data); 
    }
}

exports.Gateway$Rejected = function( data ) {
    try {
        logger.info("Gateway$Rejected: " + JSON.stringify(data), "Workflow.Engine");
        let message = data;

        if(message.data.staging_status === 'Rejected'){
            logger.info('\tLooks Good, The staged data is Rejected\n', "Workflow.Engine");
            return true;
        } else {
            logger.info('\tNo Good, do nothing', "Workflow.Engine");
            return false;
        }
    } catch (err) {
        p6Utils.handleGeneralException(data, this, err, "Error During Gateway$Rejected: ");
        done(data); 
    }
}

exports.DeleteStagedData = function( data, done ) {
    logger.info('DeleteStagedData: Submiting call to OVS to delete staged data.', "Workflow.Engine");
    logger.info('\tPublishing delete staged data message', "Workflow.Engine");
    
    // set method and queue Name
    let method = 'DELETE';
    let QUEUE_NAME = 'DeleteStagedData';
    let errorMessage = "Error Deleting the Staged Data: ";

    try {
    
        let rpcConnectionInfo = rpcUtils.rpcConnectionInfo(QUEUE_NAME);
    
        // create the template
        // request template is imported, use its GET template
        let template = rpcUtils.createTemplate( OVSTemplate[method],
                                                rpcConnectionInfo.queue,
                                                rpcConnectionInfo.routingKey,
                                                rpcConnectionInfo.exchange,
                                                data);
    
        // Create request handler first
        let request_handler = new requestHandler(template);
        
        let replyTo = data ? data.replyTo : null;

        // we need to get the id
        let idToDelete = this.getProperty('id');
        // if there is no id found, continue to the next step in workflow
        if(!idToDelete) {
            // Move along! Nothing to do because we do not have an id by which to delete
            this.taskDone('DeleteStagedData', message);
            return;
        }

        // Consuming function of the rpc call
        let consumer = (message, ack, noAck) => {
            ack(message);

            // fill in values
            request_handler.rpcCleanup(rpcConnectionInfo.queue,
                                    rpcConnectionInfo.exchange.topic,
                                    rpcConnectionInfo.routingKey,
                                    consumer)
                .then(() => {
                    let responseMessage = p6Utils.formatResponseMessage(message, this, errorMessage);

                    // If we have replyTo information, reply
                    if(replyTo){
                        request_handler.reply(replyTo, responseMessage);
                    }
                    // Remove staging id if successful
                    if(!this.getProperty('error')) {
                        this.setProperty('id', null);    
                    }
                    // The task is complete
                    this.taskDone('DeleteStagedData', message);
                });
        };

        // The message needs to be formatted a certain way for it to be staged.
        // This formats it.
        let dataToDelete = p6Utils.formatDataForDeleting(data, idToDelete);


        // Format the data to populate the request template
        let requestData = rpcUtils.formatDataForDeleteTemplateing(dataToDelete, idToDelete, method, rpcConnectionInfo);

        // begin the rpc call
        request_handler.rpcCall(consumer, requestData);
    } catch (err) {
        p6Utils.handleGeneralException(data, this, err, errorMessage);
        this.taskDone('DeleteStagedData', data);
    }
};

exports.DeleteStagedDataDone = function( data , done ) { 
    // Called after the process has been notified that the task has been finished
    // by invoking myProcess.taskDone("MyTask").
    // Note: <task name> + "Done" handler are only called for
    // user tasks, manual task, and unspecified tasks
    logger.info("\tDelete Staged Data task has been completed" + ( this.getProperty('error') ? ' with errors' : '' ) + ": \n", "Workflow.Engine");  
    done();
};

exports.PromoteData = function( data, done ) {
    logger.info('PromoteData: Submiting call to OVS to promote staged data.', "Workflow.Engine");
    logger.info('\tPublishing promote staged data message', "Workflow.Engine");

    let method = 'PUT';
    let QUEUE_NAME = 'ApprovingData';
    let errorMessage = "Error Promoting the Staged Data: ";

    try {
        let params = this.getProperty('params');
        let replyTo = data.replyTo;
        
        let rpcConnectionInfo = rpcUtils.rpcConnectionInfo(QUEUE_NAME);
    
        // create the template
        // request template is imported, use its GET template
        let template = rpcUtils.createTemplate( OVSTemplate[method],
                                                rpcConnectionInfo.queue,
                                                rpcConnectionInfo.routingKey,
                                                rpcConnectionInfo.exchange,
                                                data);
    
        // Create request handler first
        let request_handler = new requestHandler(template);

        let idToUpdate = this.getProperty('id');
        // if there is no id found, continue to the next step in workflow
        if(!idToUpdate) {
            // Big problem! Perhaps the staged data has already promoted or deleted
            // Let the user know with an error message
            this.setProperty('error', "Staged Data is not in a state awaiting an approval decision: no staging id found");
            this.taskDone('PromoteData', data);
            return;
        }

        // Consuming function of the rpc call
        let consumer = (message, ack, noAck) => {
            ack(message);

            // fill in values
            request_handler.rpcCleanup(rpcConnectionInfo.queue,
                                    rpcConnectionInfo.exchange.topic,
                                    rpcConnectionInfo.routingKey,
                                    consumer)
                .then(() => {
                    let responseMessage = p6Utils.formatResponseMessage(message, this, errorMessage);

                    // If we have replyTo information, reply
                    if(replyTo){
                        request_handler.reply(replyTo, responseMessage);
                    }
                    // Remove staging id if successful
                    if(!this.getProperty('error')) {
                        this.setProperty('id', null);    
                    }
                    // The task is complete
                    this.taskDone('PromoteData', data);
                });
        };

        // The message needs to be formatted a certain way for it to be staged.
        // This formats it.
        let dataToStage = p6Utils.formatDataForApproving(data, idToUpdate, 'Approved');

        // Format the data to populate the request template
        let requestData = rpcUtils.formatDataForApproveTemplating(dataToStage, method, rpcConnectionInfo);

        // begin the rpc call
        request_handler.rpcCall(consumer, requestData);
    } catch (err) {
        p6Utils.handleGeneralException(data, this, err, errorMessage);
        this.taskDone('PromoteData', data); 
    }
};

exports.PromoteDataDone = function( data , done ) { 
    // Called after the process has been notified that the task has been finished
    // by invoking myProcess.taskDone("MyTask").
    // Note: <task name> + "Done" handler are only called for
    // user tasks, manual task, and unspecified tasks
    logger.info("\tPromote Staged Data task has been completed" + ( this.getProperty('error') ? ' with errors' : '' ) + ": \n", "Workflow.Engine");  
    done();
};

exports.SubmissionError = function( data, done) {
    done(data);
}

exports.SubmissionError$Error = function( data, done) {
    let error = this.getProperty('error');

    if(error) {
        logger.info('error Found', "Workflow.Engine");
        return true;
    } else {
        return false;
    }
}

exports.SubmissionError$false = function( data, done) {
    let error = this.getProperty('error');

    if(error) {
        return false;
    } else {
        logger.info('no error', "Workflow.Engine")
        return true;
    }
}

exports.Resubmission = function( data , done ) {
    // called at the beginning of MyTask
    logger.info("Resubmission event: " + JSON.stringify(data), "Workflow.Engine");
    done(data);

};



/**
 * @param {String} eventType Possible types are: "activityFinishedEvent", "callHandler"
 * @param {String?} currentFlowObjectName The current activity or event
 * @param {String} handlerName
 * @param {String} reason Possible reasons:
 *                          - no handler given
 *                          - process is not in a state to handle the incoming event
 *                          - the event is not defined in the process
 *                          - the current state cannot be left because there are no outgoing flows
 */
exports.defaultEventHandler = function(eventType, currentFlowObjectName, handlerName, reason, done) {
    // Called, if no handler could be invoked.
    //logger.info('default', "Workflow.Engine");
    done(reason);
};

exports.defaultErrorHandler = function(error, done) {
    // Called if errors are thrown in the event handlers
    logger.error('default error', "Workflow.Engine");
    logger.error(error, "Workflow.Engine");
    done();
};

exports.onBeginHandler = function(currentFlowObjectName, data, done) {
    // do something
    //logger.info('begin handler: ' + JSON.stringify(data));
    done(data);
};

exports.onEndHandler = function(currentFlowObjectName, data, done) {
    // do something
    //logger.info('end handler', "Workflow.Engine");
    done(data);
}; 