/**
 * index.js
 * Entry point for AMQP
 * Initializes a consumer.
 * created by Ryann Chandler 7/25/17
 */

 //TODO: Enterprise Logging

 // Requires the files to be run
require('./main/workflow-process');
require('./main/amqp-api');
let AMQPLogger = require('./amqp_utils/amqp_logger/amqp-logger');

// Export the logger as a global module
// You can now use logger in any file
logger = new AMQPLogger();