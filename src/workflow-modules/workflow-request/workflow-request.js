/**
 * Transforms a message into a request
 */
const AMQPManager = require('../../amqp_utils/amqp-manager');
const _ = require('lodash');
const utils = require('../utils/utils');

const DefaultAMQPConfig = require('../../amqp_config/amqpConfig');
const templateCreator = require('../template-creator/template-creator');

const amqpLocation = 'amqp';
const templateLocation = {
    'consumerTemplate': 'consumerTemplate',
    'requestTemplate': 'requestTemplate'
};
const urlLocation = 'amqp_url';

class WorkflowRequest {

    /**
     * 
     * @param {Object} config - Configuration object needed for connecting to AMQP server
     */
    constructor(template, amqp_manager) {
        this.template = template;
        this.amqpManager = amqp_manager;
        this.defaultAMQPConfig = new DefaultAMQPConfig();
    }

    /**
     * Performs the act of setting up amqp dependancies and a consuemr then publishes
     * @param {Function} consumer - the consuming function
     * @param {Object} data - data to publish
     * @param {String} topic - The name of the exchange
     * @param {String} queue - The name of the queue
     * @param {String} routingKey - Name of the routingKey
     */
    rpcCall(consumer, data) {
        
        let request = this.createWorkflowRequest(data);

        // All of the connection is built into the consume and publish methods.
        return this.consume(consumer)
            .then(() => this.publish(request))
    }

    /**
     * Closes up amqp after an rpc call has been made
     * @param {String} queue - The name of the queue
     * @param {String} topic - the name of the exchange
     * @param {String} routingKey - the routing key
     * @param {Function} consumer - The consuming function to close
     */
    rpcCleanup(queue, topic, routingKey, consumer) {
        return this.amqpManager.unbindQueue(queue, topic, routingKey)
            .then(() => this.amqpManager.removeConsumer(queue))
            .then(() => this.amqpManager.deleteQueue(queue));
    }

    /**
     * 
     * @param {*} replyTo 
     * @param {*} message 
     */
    reply(replyTo, message) {
        let routing = utils.getRouting(replyTo);
        
        if (message.request_headers) {
            message.headers = _.merge(message.headers, message.request_headers);
        } else {
            message.headers = replyTo.headers;
        }

        if(routing.topic === 'topic') routing.topic = 'amq.topic';

        this.connectAndSetup(this.template)
            .then(() => {
                this.amqpManager.publish(routing.topic, null, routing.routingKey, message);
            });
    }

        /**
     * Creates a request 
     * @param {Object} requestData - All of the data in one object that can populate the dataTemplate
     */
    createWorkflowRequest(requestData) {

        // Get and Check for template
        if(!this.template) {
            throw 'No Template has been set';
        }

        let dataTemplate = this.template.requestTemplate.dataTemplate;

        let rendered = templateCreator(dataTemplate, requestData);

        return rendered;
    }

    /**
     * Get the Request or Consumer template
     * @param {String} action - How to get the Template
     */
    getTemplateConnection(action) {
        
        if(!this.template){
            throw 'Template cannot be found';
        }

        return this.template[action];

    }

    /**
     * Perform connecting and setting up the channel
     */
    connectAndSetup(template) {

        if(!this.amqpManager){
            let amqp = this.getAMQPValues(template)

            // If we have an amqpManager, use it
            // make sure amqp has url
            if(!amqp[urlLocation]){
                amqp[urlLocation] = this._getDefaultUrl();
            }
    
            // set up the new amqp manager
            this.amqpManager = new AMQPManager(amqp);
    
            return this.amqpManager.connectToServer()
                .then(() => this.amqpManager.setupChannel());
        } else {
            return Promise.resolve();
        }
    }

    /**
     * A URL is needed for connecting to the server. If one is not provided, use the default 
     */
    _getDefaultUrl(){
        return this.defaultAMQPConfig.amqp_url;
    }

    /**
     * This sequence of functions is called from multiple places.
     * It connects, creates the exchange and queue, then adds the queue binding
     * @param {Object} template - Amqp template values
     * @param {String} exchange - Name of topic
     * @param {String} queue - name of queue
     */
    amqpSetup(template, exchange, queue, routingKey) {
        return this.connectAndSetup(template)
            .then(() => this.amqpManager.createExchange(exchange.topic, exchange.type))
            .then(() => this.amqpManager.createQueue(queue))
            .then(() => this.amqpManager.addQueueBindings(queue, exchange.topic, routingKey))
    }

    /**
     * Publishes a supplied message on AMQP
     * It will set up a connection to a server and publish before it deletes itself
     * @param {Object} msg - the msg to publish ove AMQP
     */
    publish(message) {

        let requestTemplate = this.getTemplateConnection(templateLocation.requestTemplate);

        let exchange = this.getTopicFromTemplate(this.publisherAMQPConfig);

        let queue = this.getQueueFromTemplate(requestTemplate)

        let routingKey = this.getRoutingKeyFromTemplate(requestTemplate);
    
        return this.amqpManager.publish(exchange.topic, exchange.type, routingKey, message)
            .catch((err) => console.error(err));

    }

    /**
     * Adds a consumer
     * @param {String} queue - the name of the queue to consume
     * @param {Function} consumer - the function that will be called on message received
     */
    consume(consumer) {

        let consumingTemplate = this.getTemplateConnection(templateLocation.consumerTemplate);

        let queue = this.getQueueFromTemplate(consumingTemplate);

        let exchange = this.getTopicFromTemplate(consumingTemplate);

        let routingKey = this.getRoutingKeyFromTemplate(consumingTemplate);

        return this.amqpSetup(consumingTemplate, exchange, queue, routingKey)
            .then(() => this.amqpManager.addConsumer(queue, {}, consumer))
            .catch((err) => console.error(err));
    }

    /**
     * Removes a consumer from a queue
     * @param {String} queue - the name of the queue to remove from
     * @param {Function} consumer - the consumer to remove
     */
    removeConsumer(consumer) {

        let consumingTemplate = this.getTemplateConnection(templateLocation.consumerTemplate);

        let queue = this.getQueueFromTemplate(consumingTemplate); 

        return this.amqpManager.removeConsumer(queue, consumer)
            .catch((err) => console.error(err));
    }

    /**
     * gets the routing key from the template
     * @param {Object} template - template values 
     */
    getRoutingKeyFromTemplate(template) {
        if(template && template[amqpLocation]) {
            return template[amqpLocation].AMQP_REQUEST_ROUTING_KEY;
        }

        let amqp = this.getAMQPValues();
        if(amqp) {
            return amqp.AMQP_REQUEST_ROUTING_KEY;
        }

        throw 'Template does not exists.';
    }

    /**
     * Given a template, collect the queue name from the template and return
     * @param {Object} template - the template to populate values from
     * @param {*} which - either request template or consumer template
     */
    getQueueFromTemplate(template) {

        // try and get values from the template first
        if(template && template[amqpLocation]){
            return template[amqpLocation].AMQP_QUEUE_NAME;
        }

        // IF no values in template, try and use default value
        //  passed in during workflow request instantiation.
        let amqp = this.getAMQPValues();
        if(amqp) {
            return amqp.AMQP_QUEUE_NAME;
        }

        throw 'Template does not exists.';
    }

    /**
     * Gets the amqp topic from the template
     * @param {Object} template 
     */
    getTopicFromTemplate(template) {
        if(template && template[amqpLocation]) {
            return {
                topic: template[amqpLocation].AMQP_TOPIC_EXCHANGE,
                type: template[amqpLocation].AMQP_TOPIC_EXCHANGE_TYPE
            }
        }

        let amqp = this.getAMQPValues();
        if(amqp) {
            return {
                topic: amqp.AMQP_TOPIC_EXCHANGE,
                type: amqp.AMQP_TOPIC_EXCHANGE_TYPE
            }
        }
        
        throw 'Template does not exists.';
    }

    /**
     * AMQP Values can be part of the template or be initialized with the instance of the workflow-request
     * @param {Object} template - the template that stores amqp connection information
     */
    getAMQPValues(template){

        // Get values from template if they exists
        if(template && template[amqpLocation]) {
            return template[amqpLocation];
        }
        
        // if not, try and get default values
        if(this.defaultAMQPConfig) {
            return this.defaultAMQPConfig;
        }

        throw 'AMQP Config Cannot be found';
    }

    /**
     * Acknoledge the message
     * @param {Object} message 
     */
    ack(message) {
        this.amqpManager.ack(message);
    }

    /**
     * Don't acknowledge the message
     * @param {Object} message 
     */
    nack(message) {
        this.amqpManager.nack(message);
    }

    /**
     * Deletes an instance of amqpManager
     */
    removeManager() {
        delete this._amqpManager;
    }

    get amqpManager() {
        return this._amqpManager;
    }
    set amqpManager(amqpManager) {
        this._amqpManager = amqpManager;
    }

    get state() {
        return this._state;
    }
    set state(state) {
        this._state = state;
    }

    get template() {
        return this._template;
    }
    set template(template) {
        this._template = template;
    }

    get amqpConfig() {
        return this._amqpConfig;
    }
    set amqpConfig(amqpConfig) {
        this._amqpConfig = amqpConfig;
    }
}

module.exports = WorkflowRequest;