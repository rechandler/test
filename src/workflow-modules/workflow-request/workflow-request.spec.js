'use strict';

const chai = require('chai');
const expect = chai.expect;
const should = chai.should;
const assert = chai.assert;
const sinon = require('sinon');
const WorkflowRequest = require('./workflow-request');
const workflowConfig = require('../workflow-templates/OVS.template');

let workflowRequest;
beforeEach(function() {
    workflowRequest = new WorkflowRequest(workflowConfig);
});

describe('Workflow Request Module', function(){
   
    describe('instantiation', function(){
        it('should be instantiated', function(){
            expect(workflowRequest).to.be.a('Object');
            expect(workflowRequest.createWorkflowRequest).to.be.a('Function');
        });
    });

    describe('testing constructor set properties', function(){

        it('should set a template', function() {
            expect(workflowRequest.template).to.deep.equal(workflowConfig);
        });
    });

    describe('Creating a workflow request', function(){
        it('should create a workflow request', function(){

            let val = workflowRequest.createWorkflowRequest(utils.data);

            expect(val).to.deep.equal(utils.result);
        });
    });

    describe('Getting a queue from a template', function() {
        it('should return a queue name', function(){
            let val = workflowRequest.getQueueFromTemplate(utils.template.consumerTemplate);

            expect(val).to.equal('queuename');
        });
    });

    describe('publishing', function() {
        it('should try to publish', function() {
            let connectCallback = sinon.stub(workflowRequest, 'connectAndSetup').returns(Promise.resolve());
            
            workflowRequest.amqpManager = { publish: () => Promise.resolve(), 
                createExchange: () => Promise.resolve(), 
                createQueue: () => Promise.resolve() ,
                addQueueBindings: () => Promise.resolve()
            };
            workflowRequest.publish('test');

            expect(connectCallback.called);
        });
    });

    describe('consuming', function() {
        it('should set up a consumer', function() {
            let connectCallback = sinon.stub(workflowRequest, 'connectAndSetup').returns(Promise.resolve());
            
            workflowRequest.amqpManager = { addConsumer: (queue, consumer) => Promise.resolve() };

            expect(connectCallback.called);
        });
    });

    describe('removing the manager', function() {
        it('should delete the manager', function() {
            workflowRequest.amqpManager = { addConsumer: (queue, consumer) => Promise.resolve() };
            workflowRequest.removeManager();

            expect(workflowRequest.amqpManager).to.equal(undefined);
        });
    });
});

let utils = {
    args: {
        route: 'testURL',
        args: {
            id:'123',
            method: 'POST',
            endpoint: 'testEndpoint',
            params: {
                p1: 'p1'
            }
        },
        data: {
            p1: 'p1',
            p2: 'p2'
        },
        headers: {
            h1: 'h1',
            h2: 'h2'
        },
        replyTo: {
            headers: {
                rh1: 'rh1'
            }
        }
    },
    result: {
        "route": "testRoute",
        "timeout": "short",
        "endpoint": "null",
        "method": "testMethod",
        "params": {
            "resource_name": "testRoute",
            "data": "testParams",
            "method": "testMethod",
            "request_headers": {
                "type": "rpc",
                "route": "testRoute",
                "timeout": "testTimeout",
                "user": "testUser",
                "token": "testToken",
                "channelId": "testChannelId"
            },
            "request_body": {
                route: 'testRoute',
                params: 'testParams',
                method: 'testMethod',
                timeout: 'testTimeout',
                headers: {
                    user: 'testUser',
                    token: 'testToken',
                    channelId: 'testChannelId'
                },
                replyTo: 'testReplyTo'
            },
            "staging_status": "Submitted",
            "created_by": "testUser"
        },
        "headers": {
            user: 'testUser',
            token: 'testToken',
            channelId: 'testChannelId'
        },
        "replyTo": "testReplyTo"
    },
    data: {
        data: {
            route: 'testRoute',
            params: 'testParams',
            method: 'testMethod',
            timeout: 'testTimeout',
            headers: {
                user: 'testUser',
                token: 'testToken',
                channelId: 'testChannelId'
            },
            replyTo: 'testReplyTo'
        }
    },
    template: {
        "requestTemplate": {
            "dataTemplate": {
                "test": "{{ test }}"
            },
            "amqp": {
                "AMQP_QUEUE_NAME": "queueName"
            }
        },
        "consumerTemplate": {
            "consumerTest": "testing",
            "amqp": {
                "AMQP_QUEUE_NAME": "queuename"   
            }
        }
    }
};