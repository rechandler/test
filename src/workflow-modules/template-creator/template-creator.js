const regex = /{{.*}}/;
const _ = require('lodash');


/**
 * Replaces values in the template in order to create a populated AMQP request template
 * @param {Object} template - An AMQP request template. Values to be replaced are in the template
 * @param {Object} data - the object containing the data
 */
module.exports = function createTemplate(template, data) {

    let result = {};
    for(let property in template) {
        let value = template[property];

        // match the value against delimiters
        if(typeof value === 'string') {
            let found = value.match(regex);

            // Check if found, if yes, get the data.
            if(found){
                result[property] = _getDataFromLocation(found[0], data);
            } else {

                // if not, set the value
                result[property] = value;
            }
        } else if(typeof value === 'object') {

            // Recurse to create templates with nested values and data
            result[property] = createTemplate(value, data);
        }

    }

    return result;
}

/**
 * Gets data from a specific location
 * @param {String} location - {{ location }} represents a delimited location of a property
 * @param {Object} data - the data object that holds the values to be retrieved
 */
function _getDataFromLocation(location, data) {
    let cleanLocation = _getCleanLocation(location);

    return _.get(data, cleanLocation);
 }

/**
 * Takes a delimited string and takes out the delimiters and spaces.
 * {{ test1.test2 }} = 'test1.test2'
 * @param {String} location - the Location string to be cleaned.
 */
function _getCleanLocation(location) {

    // Clean up all the mustaches and spaces
    let noMustaches = location.replace(/[{}]/g, '');
    let cleanedLocation = noMustaches.trim();


    return cleanedLocation;
}
