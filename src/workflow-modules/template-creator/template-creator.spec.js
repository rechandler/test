'use strict';

const chai = require('chai');
const expect = chai.expect;
const should = chai.should;
const assert = chai.assert;
const sinon = require('sinon');

let templateCreator = require('./template-creator');

describe('templateCreator', function(){
    it('should create a template', function() {
        let val = templateCreator(utils.template, utils.data);

        expect(val).to.deep.equal(utils.result);
    });
});

let utils = {
    result: {
        "route": "testRoute",
        "timeout": "short",
        "endpoint": "null",
        "method": "testMethod",
        "params": {
            "resource_name": "testRoute",
            "data": "testParams",
            "method": "testMethod",
            "request_headers": {
                "type": "rpc",
                "route": "testRoute",
                "timeout": "testTimeout",
                "user": "testUser",
                "token": "testToken",
                "channelId": "testChannelId"
            },
            "request_body": {
                route: 'testRoute',
                params: 'testParams',
                method: 'testMethod',
                timeout: 'testTimeout',
                headers: {
                    user: 'testUser',
                    token: 'testToken',
                    channelId: 'testChannelId'
                },
                replyTo: 'testReplyTo'
            },
            "staging_status": "Submitted",
            "created_by": "testUser"
        },
        "headers": {
            user: 'testUser',
            token: 'testToken',
            channelId: 'testChannelId'
        },
        "replyTo": "testReplyTo"
    },
    data: {
        data: {
            route: 'testRoute',
            params: 'testParams',
            method: 'testMethod',
            timeout: 'testTimeout',
            headers: {
                user: 'testUser',
                token: 'testToken',
                channelId: 'testChannelId'
            },
            replyTo: 'testReplyTo'
        }
    },
    template: {
        "route": "{{ data.route }}",
        "timeout": "short",
        "endpoint": "null",
        "method": "{{data.method}}",
        "params": {
            "resource_name": "{{ data.route }}",
            "data": "{{ data.params }}",
            "method": "{{ data.method }}",
            "request_headers": {
                "type": "rpc",
                "route": "{{ data.route }}",
                "timeout": "{{ data.timeout }}",
                "user": "{{ data.headers.user }}",
                "token": "{{ data.headers.token }}",
                "channelId": "{{ data.headers.channelId }}"
            },
            "request_body": "{{ data }}",
            "staging_status": "Submitted",
            "created_by": "{{ data.headers.user }}"
        },
        "headers": "{{ data.headers }}",
        "replyTo": "{{ data.replyTo }}"
    },
}