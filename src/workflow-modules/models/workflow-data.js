class WorkflowData {
    constructor(data) {
        this.state = this.translateState(data.state);
        this.history = data.history;
        this.properties = data.properties;
        this.processId = data.processId;
    }

    /**
     * Returns the stringified State Name of the current process
     * @param {Object} data - holds the state data for a BPMN Process
     */
    translateState(data) {
        if (!data.tokens || !data.tokens.length) return;

        let state = data.tokens[0];
        return state.position;
    }
}

module.exports = WorkflowData;