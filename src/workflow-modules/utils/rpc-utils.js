const utils = require('./utils');
const AMQPConfig = require('../../amqp_config/amqpConfig');

const config = new AMQPConfig();

exports.rpcConnectionInfo = (queueName) => {

    return {
        // create a temp queue name
        // These are temp names because they are only used for making an RPC call
        // Once the call has completed (response) we will terminate the queue and exchange
        //  using the rpcCleanup function built into the request handler
        queue: utils.createTempQueueName(queueName),
        routingKey: utils.createRoutingKey(),
        exchange: utils.createExchange(queueName),
        channelId: utils.createRoutingKey() 
    }
}

/**
 * Modifies OVS template to use custom consumer properties
 * These are temporary properties because this queue will be closed at the end of 
 *  the rpc call.
 * 
 * @param {Object} template - Ovs Template Object
 * @param {String} queue - name of consuming queue
 * @param {String} routingKey - name of consuming routing key
 * @param {String} topic - name of consuming topic
 */
exports.createTemplate = (template, queue, routingKey, exchange, message) => {
    
    let user = message.replyTo.headers.user;
    let requestTemplate = template.requestTemplate;
    let requestKey = 'Request.'+ config.LIFE_CYCLE + '.' + user;

    // set the routing key in hte requestTemplate
    requestTemplate.amqp.AMQP_REQUEST_ROUTING_KEY = requestKey;
    
    // the request template needs the routing key created
    // the consumer template needs to be augmented/created on the fly
    //  reason - the consumer queue and queue  bindings are created and destroyed on an as needed basis
    //  so the name of the queue and routing key changes each time an rpc call is needed to be made
    return {
        requestTemplate: template.requestTemplate,
        consumerTemplate: {
            amqp: {
                AMQP_QUEUE_NAME: queue,
                AMQP_REQUEST_ROUTING_KEY: routingKey,
                AMQP_TOPIC_EXCHANGE: 'amq.topic',
                AMQP_TOPIC_EXCHANGE_TYPE: exchange.type 
            }
        }
    };
}

exports.getRPCHeaders = getRPCHeaders = (message, replytToHeaders, channelId, routingKey) => {

    // let replytToHeaders = message.request_headers;

    // Build a headers object using the user and token from the original message to workflow service,
    //  set the new channelId and type is rpc
    let user = replytToHeaders.user;
    let token = replytToHeaders.token;

    let headers = {
        user: user || {},
        token: token || {},
        channelId: channelId,
        type: "rpc"
    };


    // set the reply to object
    // The reply to object is used to tell data staging to reply here (context is an rpc call)
    // destination - is the routing key
    // headers - same headers created above
    let replyToHeaders = {
        destination: routingKey,
        headers: headers
    }

    return {
        headers: headers,
        replyTo: replyToHeaders
    };

}

exports.formatDataForStageTemplating = (message, method, connectionInfo) => {
    let headers = message.request_headers

    let rpcHeaders = getRPCHeaders(message, headers, connectionInfo.channelId, connectionInfo.routingKey);

    return {
        data: {
            data: message,
            params: message.request_body.params,
            method: method,
            headers: rpcHeaders.headers,
            replyTo: rpcHeaders.replyTo
        }
    };
}

exports.formatDataForApproveTemplating = (message, method, connectionInfo) => {
    
    let headers = message.data.request_body.replyTo.headers;
    
    let rpcHeaders = getRPCHeaders(message, headers, connectionInfo.channelId, connectionInfo.routingKey);    
    
    return {
        data: {
            data: message.data,
            params: message.params,
            method: method,
            headers: rpcHeaders.headers,
            replyTo: rpcHeaders.replyTo
        }
    }
}

exports.formatDataForDeleteTemplateing = (message, id, method, connectionInfo) => {
    let headers = message.request_headers
    
    let rpcHeaders = getRPCHeaders(message, headers, connectionInfo.channelId, connectionInfo.routingKey);
    
        return {
            data: {
                params: message.id,
                method: method,
                headers: rpcHeaders.headers,
                replyTo: rpcHeaders.replyTo
            }
        };
}