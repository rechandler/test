let requestHandler = require('../workflow-request/workflow-request');

/**
 * Transforms the data into what the data staging service is expecting
 */
exports.formatDataForStaging = (message, stagingStatus) => {
    let result = {};

    // set the data property
    result.data = message.data;
    result.resource_name = message.route;
    result.request_headers = message.replyTo.headers;
    result.request_body = message;
    result.method = message.method;
    result.staging_status = stagingStatus;
    result.status_code = 0;
    result.created_by = message.replyTo.headers.user;
    result.create_at = new Date();

    return result;
};

/**
 * Transforms the data into what the data staging service is expecting
 */
exports.formatDataForApproving = (message, id, stagingStatus) => {

    let result = {};
    
    let updateId = id;
    let user = message.replyTo.headers? message.replyTo.headers.user: null;
    let params = {id: updateId, updated_by: user};

    result.data = message.data;
    result.params = params;
    result.request_headers = message.headers;
    result.replyTo = message.replyTo;

    return result;
    
};

exports.formatDataForDeleting = (message, id) => {
    let result = {};

    result.request_headers = message.replyTo.headers;
    result.id = id;
    result.params = message.params;

    return result;
};

exports.formatResponseMessage = (message, workflow, errorMessage) => {
    let responseMessage = {};
    if(message.status_code && (message.status_code < 200 || message.status_code > 299)) {

        // set the error in the workflow
        workflow.setProperty('error', message.error_msg || message.message);
        console.error('error found:' + workflow.getProperty('error'));
        
        responseMessage = {
            'status_code': message.status_code,
            'message': errorMessage + (message.error_msg || message.message)
        };
    } else {
        message.status_code = !message.status_code ? 200 : message.status_code;
        responseMessage = message;
        workflow.setProperty('error', null);
    }

    return responseMessage;
}

exports.handleGeneralException = (data, workflow, err, errorMessage) => {
    logger.error(err.message);
    workflow.setProperty('error', err.message);

    // If we have replyTo information, reply
    if(data && data.replyTo) {
        let request_handler = new requestHandler();
        request_handler.reply(data.replyTo, {
            'status_code': 500,
            'message': errorMessage + err.message
        });
    }
}