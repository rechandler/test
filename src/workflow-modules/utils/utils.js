/**
 * Utilities for the Main functions of Dycom Workflow
 */

let uuidv1 = require('uuid/v1');

 /**
  * Gets the topic and routing key from the routing info
  * routingInfo.destination will always be in the form - /topic/routingKey
  */
exports.getRouting = (routingInfo) => {

    // set destination
    let destination = routingInfo.destination;

    if(!destination) throw 'Error, No Destination in replyTo';

    // split on the /
    let splitDest = destination.split('/');

    let result = {};

    // split dest should be ["", topic, routingKey];
    // ignore the first ""
    if(splitDest.length === 3) {
        result.topic = splitDest[1];
        result.routingKey = splitDest[2]
    } else {
        throw 'Error with the destination key';
    }

    return result;
}

/**
 * Creates a Queue that can be used temporarily for the sake of an RPC call
 */
exports.createTempQueueName = (name) => {
    return name + '.QUEUE.' + uuidv1();
}

/**
 * Returns a UUID that can serve as a routing key
 */
exports.createRoutingKey = () => {
    return uuidv1();
}

/**
 * Create a unique exchange topic
 */
exports.createExchange = (name) => {
    return {
        topic: name + '.TOPIC.' +  uuidv1(),
        type: 'topic'
    }
}