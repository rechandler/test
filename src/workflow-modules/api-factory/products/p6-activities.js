let BPMNAccessor = require('../../bpmn/accessor/workflow-accessor');
let rpcUtils = require('../../utils/rpc-utils');
let requestTemplate = require('../../workflow-templates/OVS.StagedData');
let request_handler = require('../../workflow-request/workflow-request');
let _ = require('lodash');

const QUEUE_NAME = 'StagedData';

class StagedDataProduct {

    constructor(amqp_manager){
        this.amqp_manager = amqp_manager;
    }

    /**
     * Get method that gathers information on a workflow and fetches staged data
     * @param {Object} message - message object recieved over AMQP
     */
    get(message){

        let withData = _.get(message, 'params.withData');

        // if with Data means get the workflow and staged data
        // if ! with Data, return all workflows associated with p6/activitites
        if(withData) {
            return this._getWorkflowData(message);
        } else {
            return this._getAssociatedWorkflows(message);
        }
    }

    /**
     * Retuns a list of workflows associated with p6 data
     * @param {Object} message - the message passed into the api
     */
    _getAssociatedWorkflows(message) {
        return new Promise((resolve, reject) => {

            //TODO: this will eventually hit mongo and return all workflows associated with p6 activities
            resolve([{
                name: 'P6 Activity Workflow',
                description: 'The Workflow for P6 Activity Data',
                process_name: 'P6_activities_staging_approval',
                id: '987654321'
            }]);
        });
    }

    _getWorkflowData(message) {
        // Set up return values
        let workflows, stagedData;
        
        // use BPMNAccessor to fetch properties
        let state = message.params? message.params.state : null;

        // make sure the state exists, if not, throw and error
        if(state){

            // instantiate BPMNAccessor
            let bpmnAccessor = new BPMNAccessor('src/assets/p6_activities_staging_approval.bpmn');

            // use the bpmn accessor to get all by state
            return bpmnAccessor.getByState(message.params.state)
                .then((results) => {

                    // set the workflows
                    workflows = results;

                    // transform a list of worklows into a list of id's
                    //  this is to make formulating the request object easier
                    let params = this._getWorkflowIds(workflows);

                    // use the params to get the staged data
                    return this._getStagedWorkflowData(message, params);
                })
                .then((results) => {

                    // set the staged data object
                    stagedData = results;
                    // join the staged data with the results
                    return this._joinResults(workflows, stagedData);
                });
        } else {
            throw 'No state to search workflows for';
        }
    }

    /**
     * Joins workflow objects with it's staged data
     * @param {Array} workflows - list of workflow objects
     * @param {Array} stagedData - list of staged Data
     */
    _joinResults(workflows, stagedData) {
        let results = [];

        // let the workflow drive the joining
        for(let workflow of workflows) {
            let stagingId = workflow.properties.id;

            // successful find is on the id of the staged data and the worfklow properties id
            let workflowData = _.find(stagedData, (sd) => sd.id === stagingId);

            // push the new object into the results
            results.push({
                workflow: workflow,
                stagedData: workflowData
            });
        }
        return results;
    }

    /**
     * Uses the id from the found workflow to fetch the staged data from the data stagind API
     * @param {BPMNProcess} workflow - The BPMNProcess. Use the Id to get the staged Data
     */
    _getStagedWorkflowData(message, params) {
        return new Promise((resolve, reject) => {

            // if params is an empty array, then no staging ids were found
            // In this case we should resolve with an empty array for staging data
            if(params.length == 0) {
                resolve([]);
                return;
            }

            let method = 'GET';

            // Get unique connection info
            let rpcConnectionInfo = rpcUtils.rpcConnectionInfo(QUEUE_NAME);

            // create the template
            // request template is imported, use its GET template
            let template = rpcUtils.createTemplate(requestTemplate[method], 
                                                   rpcConnectionInfo.queue, 
                                                   rpcConnectionInfo.routingKey, 
                                                   rpcConnectionInfo.exchange,
                                                   message);

            // Create the request handler
            let requestHandler = new request_handler(template, this.amqp_manager);            

            // Set up consuming function to handle the result of staged data
            let consumer = (message, ack, noAck) => {
                ack(message);
    
                // Perform necessary cleanup
                requestHandler.rpcCleanup(rpcConnectionInfo.queue, 
                                          rpcConnectionInfo.exchange.topic, 
                                          rpcConnectionInfo.routingKey, 
                                          consumer)
                    .catch(err => console.error(err));


                // Check the status of the message
                if(message.status === 200 || !message.status){
                    // Resolve if good
                    resolve(message);
                } else {
                    // Reject if bad
                    reject(message);
                }
            };

            // setting up the data may be unique to each 'product'.
            // this is why its here and not rpc utils
            let data = this._createRequestData(message, 
                                               params, 
                                               rpcConnectionInfo.exchange, 
                                               rpcConnectionInfo.routingKey, 
                                               rpcConnectionInfo.channelId, 
                                               method);
                
            // make an rpc call with the consumer and the data;
            requestHandler.rpcCall(consumer, data);
        });
    }

    /**
     * formats data so we can populate a template
     * 
     * Template Object
     *      {
     *          "route": "datastagings",
     *          "timeout": "system",
     *          "method": "GET",
     *          "params": { 
     *            "id": "{{ data.params }}"
     *          },
     *          "headers": "{{ data.headers }}",
     *          "replyTo": "{{ data.replyTo }}" 
     *      }
     * @param {Object} message - message data
     * @param {Object} exchange - Exchange Object
     * @param {String} routingKey 
     */
    _createRequestData(message, params, exchange, routingKey, channelId, method) {

        // Build a headers object using the user and token from the original message to workflow service,
        //  set the new channelId and type is rpc
        let rpcHeaders = {
            user: message.replyTo.headers.user || {},
            token: message.replyTo.headers.token || {},
            channelId: channelId,
            type: "rpc"
        };

        // set the reply to object
        // The reply to object is used to tell data staging to reply here (context is an rpc call)
        // destination - is the routing key
        // headers - same headers created above
        let replyTo = {
            destination:  routingKey,
            headers: rpcHeaders
        };

        //TODO: get method add to data

        // wrap in 'data' property to allow easier parsing in template creator
        return { 
            data: {
                method: method,
                params: params,
                state: message.params.state, 
                route: message.params.route, 
                headers: rpcHeaders, 
                replyTo: replyTo
            }
        };   
    }

    /**
     * For each workflow passed in, add the workflow ID to an array
     * This is used for formatting the params of the template to 
     * request data from the data staging service
     * @param {Object} workflows - List of workflow Objects
     */
    _getWorkflowIds(workflows) {
        let results = [];
        for(let workflow of workflows) {
            if(workflow.properties && workflow.properties.id) {
                results.push(workflow.properties.id);
            }
        }

        return results;
    }
}

module.exports = StagedDataProduct;