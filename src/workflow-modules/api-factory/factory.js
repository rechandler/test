let p6Product = require('./products/p6-activities');

class Factory {

    constructor(amqp_manager) {
        this.amqp_manager = amqp_manager;
    }

    /**
     * perform a get operations
     * @param {String} route - The route of product to use
     * @param {Object} message - The message to be handled
     */
    get(resource, message){
        switch(resource) {
            case "p6/activities":
                return new p6Product(this.amqp_manager).get(message);
                break;
            case "workflows_names":
                break; 
            default:
                break;
        }
    }
    
    /**
     * perform a post operations
     * @param {String} route - The route of product to use
     * @param {Object} message - The message to be handled
     */
    post(route, message) {
        console.log('post');
        return Promise.resolve();
    }
    
    /**
     * perform a put operations
     * @param {String} route - The route of product to use
     * @param {Object} message - The message to be handled
     */
    put(route, message) {
        console.log('put');
        return Promise.resolve();
    }

    /**
     * perform a delete operations
     * @param {String} route - The route of product to use
     * @param {Object} message - The message to be handled
     */
    delete(route, message) {
        console.log('delete');
        return Promise.resolve();
    }
}

module.exports = Factory;