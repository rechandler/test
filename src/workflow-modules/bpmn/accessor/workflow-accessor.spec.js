'use strict';

const chai = require('chai');
const expect = chai.expect;
const should = chai.should;
const assert = chai.assert;
const sinon = require('sinon');
const workflowAccessor = require('./workflow-accessor');

let wfa;    
beforeEach(function(){
    wfa = new workflowAccessor(utils.filePath);
});

describe('Workflow Accessor', function(){
    it('should return a workflow', function(done) {
        wfa.getByProperties(utils.props)
            .then((result) => {

                //console.log(result);
                expect(result).to.be.a('Array');
                
                done();
            })
            .catch((err) => console.error(err));
    });

    it('should flatten the object', function() {
        let val = wfa.flattenQuery(utils.props);

        expect(val).to.deep.equal(utils.flatResult);
    })
});

let utils = {
    props: {
        "id" : "12345",
        "route" : "p6/activities",
        "params" : {
            "projectObjectId" : 9,
            "id" : "12345",
            "ObjectId" : 287492
        }
    },
    flatResult: {
        "id": "12345",
        "route": "p6/activities",
        "params.projectObjectId": 9,
        "params.id": "12345",
        "params.ObjectId": 287492
    },
    filePath: 'src/assets/p6_activities_staging_approval.bpmn'
}