let bpmn = require('bpmn');
let path = require('path');
let WorkflowModel = require('../../models/workflow-data');
let DBConfig = require('../../../db-config')
let dbConfig = new DBConfig();

class WorkflowAccessor {
    constructor(filePath) {
        this.processManager = this.createManager();

        // get the path to the bpmn file
        let flowPath = path.resolve(filePath);
        
        // add the workflow file to the process manager 
        try{
            this.processManager.addBpmnFilePath(flowPath)           
        } catch(error){
            console.log(error); 
        }
    }

    /**
     * Finds a workflow by properties
     * @param {Object} properties - parameters used to find the correct workflow instance
     */
    getByProperties(properties) {
        return new Promise((resolve, reject) => {

            // flatten the properties so we can successfully query 
            let query = this.flattenQuery(properties);

            this.processManager.findByProperty(query, (err, processes) => {
                if(err) {
                    reject(err)
                }

                let formattedProcess = this.formatProcess(processes);

                resolve(formattedProcess);
            });
        });
    }

    /**
     * Get all processes by name of state
     * @param {String} state - The name of the state
     */
    getByState(state) {
        return new Promise((resolve, reject) => {
            this.processManager.findByState(state, (err, processes) => {
                if(err) {
                    reject(err);
                }

                let formattedProcesses = this.formatProcess(processes);

                resolve(formattedProcesses);
            });
        });
    }

    /**
     * Gets a process by an id
     * @param {String} processId - The process ID to fetch
     */
    getById(processId) {
        return new Promise((resolve, reject) => {

            this.processManager.get(processId, (err, process) => {
                if(err) {
                    reject(err);
                }

                // format the workflow to be consumable
                let formattedProcess = this.formatWorkflow(process);
                
                resolve(formattedProcess);
            });
        });
    }

    /**
     * Formats an array of process to be consumable
     * @param {BPMNProcess} processes 
     */
    formatProcess(processes) {
        
        // check that we have an array
        if(Array.isArray(processes)) {
            let result = [];
            for(let item of processes) {

                // _implementation is the location of the data returned from the process manager
                result.push(this.formatWorkflow(item._implementation));
            }
            return result;
        } else {
            throw 'Data is not Array';
        }
    }

    /**
     * Formats a BPMNProcess to be consumable
     * @param {Object} dataLayer 
     */
    formatWorkflow(dataLayer) {

        return new WorkflowModel(dataLayer);
    }

    /**
     * Takes an object and flattens it so that everything is one layer deep
     * @param {Object} properties - An object that is flattened to be one layer deep
     */
    flattenQuery(properties) {
        var result = {};
        
        // for each property of the property object
        for (var prop in properties) {
            
            // Set the value
            let value = properties[prop];

            // is it an object and not null.
            if ((typeof value) === 'object' && value !== null) {

                // recurse with new object
                var flatObject = this.flattenQuery(value);
                for (var flatProp in flatObject) {
                    
                    // loop through flat object properties
                    result[prop + '.' + flatProp] = flatObject[flatProp];
                }
            } else {

                // set the value
                result[prop] = properties[prop];
            }
        }
        return result;
    }

    createManager() {
        return new bpmn.ProcessManager({
            persistencyOptions: dbConfig.options
        });
    }
}

module.exports = WorkflowAccessor;