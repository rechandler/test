'use strict';

const chai = require('chai');
const expect = chai.expect;
const should = chai.should;
const assert = chai.assert;
const sinon = require('sinon');
const bpmn = require('./bpmn-runner');


let bpmnTester;
beforeEach(function(){
    bpmnTester = new bpmn();
});

describe('BPMN Runner Module', function(){
    describe('Instantiate the module', function(){
        it('BPMN Runner Should be instantiated', () => {
            expect(bpmnTester).to.be.a('Object');
            expect(bpmnTester.run).to.be.a('Function');
        });
    });

    describe('Running the Module', function(){
        it('should run the BPMN module', function(){
            let fileCallback = sinon.stub(bpmnTester.processManager, 'addBpmnFilePath');
            let createCallback = sinon.stub(bpmnTester, 'createProcess');
            let processCallback = sinon.stub(bpmnTester, 'getCurrentProcess').resolves(false);

            bpmnTester.run({'headers': {'processId': 'tester'}});

            assert(fileCallback.called);
            assert(processCallback.called);
        });
    });

    describe('Get Current Process Id', function() {
        it('Should Return a promise', function() {
            expect(bpmnTester.getCurrentProcess()).to.be.a('Promise');
        });
    });
});