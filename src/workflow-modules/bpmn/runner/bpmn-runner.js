/**
 * BPMN Engine - This is a wrapper class for the e2e/bridge/bpmn library
 * https://github.com/e2ebridge/bpmn
 * Created by Ryann Chandler 7/26/17
 */

let bpmn = require('bpmn');
let path = require('path');
let DBConfig = require('../../../db-config')
let dbConfig = new DBConfig();

class BPMNEngine {
    constructor() {
        this.processManager = this.createManager();
    }

    // ### run
    /**
     * Run the BPMN Workflow
     */
    run(data) {

        // get the path to the bpmn file
        let flowPath = path.resolve('src/assets/p6_activities_staging_approval.bpmn');
        
        // add the workflow file to the process manager 
        try{
            this.processManager.addBpmnFilePath(flowPath)           
        } catch(error){
            console.log(error); 
        }
        
        let workflowId = data.headers? data.headers.processId: null;

        if(workflowId){
            // make sure to stringify the workflowID
            if(typeof workflowId !== 'string'){
                workflowId = workflowId.toString();
            }
    
             // try and fetch an in progress workflow
            return this.getCurrentProcess(workflowId)
            .then((foundProcess) => {
                // if process is found, work off current process
                if(foundProcess){
                    return this.handleInProgress(foundProcess, data);
                } else {
                    // else create the process
                    return this.createProcess(workflowId, data);
                }
            })
            .catch((err) => logger.error(err));
        } else {
            Promise.reject('No Workflow id');
        }
    }

    // ### getCurrentProcess
    /**
     * Uses the dataId to try and get a process
     * @param {*} dataId 
     */
    getCurrentProcess(dataId) {
        return new Promise((resolve, reject) => {

            // use the process manager to get the process
            this.processManager.get(dataId, (err, foundProcess) => { 
                resolve(foundProcess);
            });
        });
    }

    // ### handleInProgress
    /**
     * 
     * @param {* If a workflow is in progress, handle it accordingly
     * @param {*} currentProcess - the current process to work currentProcess 
     * @param {*} data 
     */
    handleInProgress(currentProcess, data) {
        let currentState = currentProcess.getState();

        let currentPosition = this.getCurrentPosition(currentState);
        currentProcess.triggerEvent(currentPosition, data);
        return Promise.resolve();
    }

    getCurrentPosition(currentState){
        return currentState.tokens[0].position;
    }

    // ### createProcess
    /**
     * If a workflow does not yet exists. Create the workflow
     */
    createProcess(workflowId, data){
        return new Promise((resolve, reject) => {
            console.log('\nBPMN Process Runner Data Recieved: ', data);
            
            // Else create the process
            this.processManager.createProcess(workflowId, (err, workflowProcess) => {
                if(err){
                    reject(err);
                } else {
                    // trigger the event
                    workflowProcess.triggerEvent("Start", data); 
                    resolve();
                }
            });
        });
    }

    // ### createManager
    /**
     * create the Process Manger and tell it to use mongo
     */
    createManager() {
        return new bpmn.ProcessManager({
            persistencyOptions: dbConfig.options
        });
    }

    // ### getAllProesses
    /**
     * Gets all of the workflows in process
     */
    getAllProcesses() {
        this.processManager.getAllProcesses((err, processes) => {
            console.log(processes);
        });
    }
}

module.exports = BPMNEngine;