/**
 * index.js
 * Entry point for AMQP
 * Initializes a consumer.
 * created by Ryann Chandler 7/25/17
 */

 //TODO: Enterprise Logging

 // Requires

let amqpConfig = require('./amqp_config/amqpConfig');
let AMQPManager = require('./amqp_utils/amqp-manager');
let bpmn = require('./workflow-modules/bpmn/runner/bpmn-runner');

// instantiate new config and consumer modules
let config = new amqpConfig();

// instantiate BPMN
let bpmnRunner = new bpmn();

/**
 * This function simulates messages that follow the logic for the workflow being tested.
 * @param {*} amqp_manager 
 */
let workflowTest = (amqp_manager) => {
    let id = null, staging_data = null, data = null;
    setTimeout(() => {
        // Send the negative test for p6/activities update - should be counted as a "No" in the P6Test
        id = '12345';
        staging_data = {id: id, route: 'p6/activities', params: {projectObjectId: 9, id: id, ObjectId: 287492}};

        amqp_manager.publish(config.AMQP_TOPIC_EXCHANGE, config.AMQP_TOPIC_EXCHANGE_TYPE, config.AMQP_REQUEST_ROUTING_KEY, staging_data);
    }, 1000);

    setTimeout(() => {
        // Send the positive test for data_staging with status 'Submitted'
        data = {id: id, route: 'data_staging', params: {id: id, resource_name:'p6/activities', staging_status: 'Submitted', data: staging_data}};
        amqp_manager.publish(config.AMQP_TOPIC_EXCHANGE, config.AMQP_TOPIC_EXCHANGE_TYPE, config.AMQP_REQUEST_ROUTING_KEY, data);
    }, 6000);

    setTimeout(() => {
        // Send the positive test for data_staging with status 'Rejected'
        data.params.staging_status = 'Rejected';
        amqp_manager.publish(config.AMQP_TOPIC_EXCHANGE, config.AMQP_TOPIC_EXCHANGE_TYPE, config.AMQP_REQUEST_ROUTING_KEY, data);
    }, 9000);

    setTimeout(() => {
        // Send the positive test for resubmitting data
        amqp_manager.publish(config.AMQP_TOPIC_EXCHANGE, config.AMQP_TOPIC_EXCHANGE_TYPE, config.AMQP_REQUEST_ROUTING_KEY, staging_data);
    }, 12000);

    setTimeout(() => {
        // Send the positive test for data_staging with status 'Approved'
        data.params.staging_status = 'Approved';
        amqp_manager.publish( config.AMQP_TOPIC_EXCHANGE, config.AMQP_TOPIC_EXCHANGE_TYPE, config.AMQP_REQUEST_ROUTING_KEY, data);

    }, 15000);      

    setTimeout(() => {
        amqp_manager.removeConsumer(config.AMQP_QUEUE_NAME, config.AMQP_TOPIC_EXCHANGE_TYPE, consumerCallback);
    }, 17000);

}

/**
 * We pass this function into the AMQP manager. This allows for a more
 * flexible amqp manager. This function is run when a message is picked up
 * off of a queue. We control the logic for the workflow Service here.
 * @param {*} message 
 * @param {*} ack 
 * @param {*} noAck 
 */
let consumerCallback = (message, ack, noAck) => {
    // acknowledge the message
    ack(message);

    bpmnRunner.run(message);
}

let amqp_manager = new AMQPManager(config);

amqp_manager.connectToServer()
    .then(() => amqp_manager.setupChannel())
    .then(() => amqp_manager.createExchange(config.AMQP_TOPIC_EXCHANGE, config.AMQP_TOPIC_EXCHANGE_TYPE))
    .then(() => amqp_manager.createQueue(config.AMQP_QUEUE_NAME))
    .then(() => amqp_manager.addQueueBindings(config.AMQP_QUEUE_NAME, config.AMQP_TOPIC_EXCHANGE, config.AMQP_REQUEST_ROUTING_KEY))
    .then(() => amqp_manager.addConsumer(config.AMQP_QUEUE_NAME, {}, consumerCallback))
    .then(() => workflowTest(amqp_manager))
    .catch((err) => console.error('error ', err));