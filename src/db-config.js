/**
 * DB Config - This is a config class for returning the mongo db uri
 * Created by Ron Roth 9/14/17
 */

class DBConfig {
    constructor() {
        this.deployed = false;
        this.mongoHost = 'localhost';
        this.mongoPort = '27017';
        this.mongoDb = 'dycom_workflow';
        this.mongoUsername = '';
        this.mongoPassword = '';
        this.mongoReplicaSet = '';
    }

    get options() {
        let options = {
            uri: 'mongodb://' +
                (this.deployed ? this.mongoUsername + ':' + this.mongoPassword + '@' : '' ) + 
                this.mongoHost + '/' + this.mongoDb
        };
        if(this.deployed) {
            options.replicaSet = this.mongoReplicaSet;
        }
        return options;
    }
}

module.exports = DBConfig;