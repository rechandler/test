Installation: 
    1. Change directories to be at the root of dycom_workflow
    2. At the command line, run "npm install"

Run:
    1. At the command line run 'npm start'
        This runs src/index.js which is the entry point for our rabbitMQ connector.
        Nodemon is used to allow for live reloading while developing
    2. start mongo instance

Test:
    1. Change directories to be at the root of dycom_workflow
    2. To run unit test: at the command line run `npm test`
    3. To run workflow simulation: at the command line run `npm run workflow`